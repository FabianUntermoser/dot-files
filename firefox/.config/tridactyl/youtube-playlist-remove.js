// Javascript Hook for Tridactyl
// For youtube.com domains

// button to add to currently watching video to a playlist
// find selector by searching for an element with the text "Save" / "Speichern",
// right click -> copy CSS selector

function closePopup() { document.querySelector('#close-button').click(); }
function removeFromPlaylist() {
    // button to add/remove from watch later playlist
    const checkboxSelector = '#checkbox #checkbox'
    if (document.querySelector(checkboxSelector).classList.contains('checked')) {
        document.querySelector(checkboxSelector).click()
    }
}

for (const element of document.querySelectorAll('button')) {
    if (element.innerText.match("Speichern|Save"))  {

        // open playlist popup window
        element.click();

        setTimeout(() => {
            removeFromPlaylist()
            setTimeout(closePopup, 200);
        }, 700);

        break;
    }
}
