// Javascript Hook for Tridactyl
// copies correctly formatted markdown link from website to clipboard

function get_link() {
    // strip away appended URL from extension
    return document.title.replace(/\s\|\shttp.*/g, "")
}

function format_link(url, title) {

    if (url.includes("youtube.com")) {
        const videoTitle = document.querySelector("h1.ytd-watch-metadata > yt-formatted-string:nth-child(1)").innerText;
        const idxTrim = url.indexOf("&list=WL")
        const rawLink = idxTrim === -1 ? url :
            url.slice(0, url.indexOf("&list=WL"))
        return `[Youtube: ${videoTitle}](${rawLink})`;
    }

    if (url.includes('github.com')) {
        return `[GitHub: ${title}](${url})`;
    }

    if (url.includes('gitlab.')) {
        title = title.slice(0, title.indexOf(" · GitLab"));
        title = title.slice(0, title.indexOf(" · Merge requests"));
        return `[GitLab: ${title}](${url})`;
    }

    if (url.includes("gerrit-review")) {
        title = title.slice(0, title.indexOf(" · Gerrit Code Review"));
        return `[Gerrit: ${title}](${url})`
    }

    if (url.includes("docs.google.com/spreadsheets")) {
        title = title.slice(0, title.indexOf(" - Google Sheets"));
        return `[GSheet: ${title}](${url})`
    }

    if (url.includes("docs.google.com/presentation")) {
        title = title.slice(0, title.indexOf(" - Google Slides"));
        return `[GSlide: ${title}](${url})`
    }

    if (url.includes("docs.google.com/document")) {
        title = title.slice(0, title.indexOf(" - Google Docs"));
        return `[GDoc: ${title}](${url})`
    }

    if (url.includes("calendar.google.com/calendar")) {
        title = title.slice(0, title.indexOf("Google Calendar - "));
        return `[GCal: ${title}](${url})`
    }

    if (url.includes("synology.me/photo")) {
        title = document.querySelector('.synofoto-photowall-album-name').innerText;
        return `[Synology Photos: ${title}](${url})`
    }

    if (url.includes("chatgpt.com/")) {
        return `[GPT: ${title}](${url})`
    }

    return `[${title}](${url})`;
}

function getSelectionText() {
    const selection = window.getSelection()

    let selectionText = "";
    const selectedFragments = selection.getRangeAt(0).cloneContents().childNodes;

    for (const fragment of selectedFragments) {
        if (fragment.outerHTML) selectionText += fragment.outerHTML;
        else if (fragment.textContent) selectionText += fragment.textContent;
        else if (fragment.innerText) selectionText += fragment.innerText;
    }

    return selectionText
}

function formatText(text, link) {
    return text === "" ? link : `> ${text} — ${link}`
}

const url = window.location.href;
const title = get_link()
const link = format_link(url, title)
const selectionText = getSelectionText()
const text = formatText(selectionText, link)
navigator.clipboard.writeText(text);
