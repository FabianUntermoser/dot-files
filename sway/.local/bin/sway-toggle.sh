#!/usr/bin/env bash

# USAGE: toggle focus between application and previously focused window

app_id=${1:?"missing app_id"}
echo "app_id: $app_id"

get_focused() { swaymsg -r -t get_tree | jq -r '.. | select(.focused?)'; }
get_focused_id() { get_focused | jq -r '.id'; }
get_focused_class() { get_focused | jq -r '.window_properties.class'; }

# test: app already focused
# swaymsg "[class=\"$1\"] focus"
# get_focused | jq '.window_properties.class'

# check if window already focused
if [[ $(get_focused_class) == $app_id* ]]; then
  # window already focused
  swaymsg "[con_mark=_prev] focus"
else
  # window not focused
  swaymsg "[class=\"$1\"] focus"
fi
