#!/usr/bin/env bash

# USAGE: setup virtual monitor and audio source for screen recording

log(){ echo -e "\e[1minfo:\e[0m $*"; notify-send "$*"; }
err(){ echo -e "\e[31merror:\e[0m $*" >&2 ; notify-send "error: $*"; }
die(){ err "$*"; exit 1; }
launch(){ setsid -f "$@" >/dev/null; }

setup() {
    setup_audio
    setup_screen
    setup_apps
    setup_workspace
}

destroy() {
    destroy_screen
    destroy_audio
    destroy_workspace
}

setup_audio() {
    log "creating virtual audio sink"
    # current device/sink to feed audio from virtual output back to
    active_sink=$(pactl list short sinks | grep RUNNING | awk '{print$2}')

    [ -z "${active_sink}" ] && {
        err "could not detect current audio sink"
        return
    }

    # create virtual audio device
    pactl load-module module-combine-sink sink_name=combined slaves="$active_sink" >/dev/null ||
        err "could not create virtual audio sink for $active_sink"
}

setup_screen() {
    log "creating virtual screen"
    headless_display=$(swaymsg -t get_outputs -r | jq -r -e '.[].name | select(test("HEADLESS"))');
    [ -z "$headless_display" ] && swaymsg create_output
}

setup_apps() {
    log "launching applications"
    pidof -q obs || launch obs
    pidof -q teams-for-linux || launch /opt/teams-for-linux/teams-for-linux
}

setup_workspace() {
    # move native teams to virtual desktop
    log "moving teams to virtual desktop"
    swaymsg -q '[class="teams-for-linux"] move container to workspace HEADLESS, floating disable, fullscreen enable'

    log "disable notifications"
    makoctl mode -a do-not-disturb; pkill -RTMIN+11 waybar
}

destroy_audio() {
    log "remove virtual audio device"
    pactl unload-module module-combine-sink
}

destroy_screen() {
    headless_display=$(swaymsg -t get_outputs -r | jq -r -e '.[].name | select(test("HEADLESS"))');
    log "remove virtual screen $headless_display"
    [ -n "$headless_display" ] && swaymsg output "$headless_display" unplug
}

destroy_workspace() {
    log "enable notifications"
    makoctl mode -r do-not-disturb; pkill -RTMIN+11 waybar
}

case $1 in
    --on) setup ;;
    --off) destroy ;;
    *) echo "Usage: [--on|--off]"
esac
