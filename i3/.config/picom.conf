## Backend

# Backend to use: "xrender" or "glx".
# GLX backend is typically much faster but depends on a sane driver.
# backend = "xrender";
backend = "glx";

## GLX Backend

# performance boost
glx-no-stencil = false;

## Animations

animations = false;
animation-stiffness = 300.0;
animation-dampening = 22.0;
animation-clamping = true;
animation-mass = 1;
#available options: zoom, fly-in, slide-up, slide-down, slide-left, slide-right
animation-for-open-window = "fly-in";
animation-for-menu-window = "zoom";
animation-for-transient-window = "zoom";
animation-for-workspace-switch-in = "zoom";
animation-for-workspace-switch-out = "zoom";

## Appearance

shadows = true
shadow-radius = 7;
shadow-offset-x = -7;
shadow-offset-y = -7;
shadow-exclude = [
    "! name~=''",
];

# Dim inactive windows. (0.0 - 1.0)
inactive-dim = 0.15;

# Fade windows during opacity changes.
fading = true;
# The time between steps in a fade in milliseconds. (default 10).
fade-delta = 3;
# Opacity change between steps while fading in. (default 0.028).
fade-in-step = 0.03;
# Opacity change between steps while fading out. (default 0.03).
fade-out-step = 0.03;

wintypes:
{
  normal = { fade = false; shadow = true; }
  tooltip = { fade = true; shadow = true; opacity = 0.75; focus = true; full-shadow = false; };
  dock = { shadow = false; }
  dnd = { shadow = false; }
  popup_menu = {}
  dropdown_menu = {}
};

# requires: https://github.com/sdhand/compton or https://github.com/jonaburg/picom
corner-radius = 5.0;
rounded-corners-exclude = [
    "window_type = 'dock'"
];

# invert colors on all windows that are tagged
invert-color-include = [
    "TAG_INVERT@:8c = 1"
];
