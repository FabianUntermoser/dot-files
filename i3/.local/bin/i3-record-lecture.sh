#!/usr/bin/env bash

# USAGE: setup virtual monitor and audio source for screen recording

log(){ echo -e "\e[1minfo:\e[0m $*"; notify-send "$*"; }
err(){ echo -e "\e[31merror:\e[0m $*" >&2 ; notify-send "error: $*"; }
die(){ err "$*"; exit 1; }
launch(){ setsid -f "$@" >/dev/null; }

create_virtual_audio_device() {
    # current device/sink to feed audio from virtual output back to
    active_sink=$(pactl list short sinks | grep RUNNING | awk '{print$2}')

    [ -z "${active_sink}" ] && {
        err "could not detect current audio sink"
        return
    }

    # create virtual audio device
    # note that adding multiple sinks is buggy
    pactl load-module module-combine-sink sink_name=combined slaves="$active_sink" \
        >/dev/null || err "could not create virtual audio sink for $active_sink"
}

setup_audio() {
    log "stopping picom"
    killall picom

    log "creating virtual audio sink"
    create_virtual_audio_device
}

setup_screen() {
    log "creating virtual screen"
    i3-virtualscreen.sh --on down 1000
}

start_apps() {
    log "reconfigure wacom tablet"
    wacom.sh

    log "launching teams and obs"
    pidof -q obs || launch obs
}

setup_workspace() {
    # move native teams to virtual desktop
    pidof -q teams && {
        log "moving teams to virtual desktop"
        i3-msg -q '[class="Microsoft Teams*"] move container to workspace TEAMS, floating disable'
        i3-msg -q 'workspace TEAMS; move workspace to output down'
        i3-msg -q 'workspace back_and_forth'
    }

    # start obs project window to preview virtual screen
    for (( i=1 ; i<=5 ; i++ )); do
        # TODO: start watcher to open OBS window previewer if it closes
        obs-cli OpenProjector >/dev/null 2>&1 && continue
        sleep 1
    done

    log "disable notifications"
    dunstctl set-paused true && polybar-msg hook dunst 2
}

setup() {
    setup_audio
    setup_screen
    start_apps
    setup_workspace
}

destroy_audio() {
    log "remove virtual audio device"
    pactl unload-module module-combine-sink
}

destroy() {
    log "remove virtual screen"
    i3-virtualscreen.sh --off >/dev/null

    log "reconfiguring wacom tablet"
    wacom.sh

    # log "closing apps"
    # i3-msg -q '[class="fh-teams*"] kill'
    # i3-msg -q '[class="Microsoft Teams*"] move scratchpad'
    # i3-msg -q '[class="obs" title="Windowed Projector*"] kill'

    destroy_audio

    log "starting picom"
    picom

    log "disable notifications"
    dunstctl set-paused false && polybar-msg hook dunst 1
}

case $1 in
    --on) setup ;;
    --off) destroy ;;
    *) echo "Usage: [--on|--off]"
esac
