#!/usr/bin/sh

VERBOSE=false

debug() { [ "$VERBOSE" = true ] && notify-send "i3-monitor: $*"; }
die() { debug "$@"; exit; }
start() { i3-msg -q "exec --no-startup-id \"$1\""; }
parse_event_type() { jq '.change'; }
parse_wm_class() { jq '.container.window_properties.class'; }

help() {
    while read -r; do printf '%s\n' "$REPLY"; done <<-EOF
	Usage:
	  ${0##*/} [-h] [-v] [-e CMD] [-w WAIT] [-t TYPE] [-r RUN] [-c CLASS]

	Monitor i3 window events and run i3 commands on matching events.

	Options:
	  -h | --help          -> print this help
	  -v | --verbose       -> verbose mode
	  -e | --execute CMD   -> application to launch with i3
	  -w | --wait WAIT     -> timout until monitoring starts
	  -t | --type TYPE     -> match event by event type
	  -r | --run RUN       -> i3 command to run on matched event
	  -c | --class CLASS   -> match event by WM_CLASS
	EOF
exit 0;
}

# parse_xwindow_id() { sed -n 's/.*"window":\([0-9]\+\).*/\1/p'; }
# slower version:
parse_xwindow_id() { jq '.container.window'; }
parse_args() {
    [ -z "$1" ] && help
    for arg in "$@"; do
        case $arg in
            -v | --verbose) VERBOSE="true"; shift; ;;
            -e | --execute) CMD="$2"; shift 2; ;;
            -w | --wait) WAIT="$2"; shift 2; ;;
            -t | --type) TYPE="$2"; shift 2; ;;
            -r | --run)  RUN="$2"; shift 2; ;;
            -c | --class) CLASS="$2"; shift 2; ;;
            -h | --help | -*) help ;;
        esac
    done;
}

parse_args "$@"
[ -z "$RUN" ] && help;
[ -z "$CLASS" ] && help;
[ -z "$TYPE" ] && help;

# run command
[ -n "$CMD" ] && i3-msg "exec --no-startup-id $CMD"

# exit if already running
pgrep -a "${0##*/}" |
    sed "/$$/d" |       # remove pid of current instance
    grep -w "$TYPE" |   # filter for event TYPE
    grep -w "$RUN" |    # filter for RUN command
    grep -w "$CLASS" |  # filter for our CLASS
    grep -v "$CLASS \w" && die "Already Monitoring. Stopping now"

debug "For class: '$CLASS', on event type: '$TYPE', run: '$RUN'"

[ -n "$WAIT" ] && {
    debug "Waiting for $WAIT seconds"
    sleep "$WAIT"
    debug "Finished waiting. For class: '$CLASS', on event type: '$TYPE', run: '$RUN'"
}

i3-msg -m -t subscribe '[ "window" ]' |
    while IFS= read -r event; do
        event_type=$(echo "$event" | parse_event_type)
        event_class=$(echo "$event" | parse_wm_class)

        [ "$event_class" = "\"$CLASS\""  ] || continue
        [ "$event_type" = "\"$TYPE\""  ] || continue

        wm_id=$(echo "$event" | parse_xwindow_id)
        debug "Running: $RUN"
        i3-msg -q "[id=$wm_id] $RUN";
    done
