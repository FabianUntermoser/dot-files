#!/bin/bash

# This script is not supposed to be run as a standalone script.
# It is run from the `workspace.sh` script.

wait_for_network || exit

CLOUD_DIR="${CLOUD_DIR:-$HOME/cloud}"
REPO_DIR="${REPO_DIR:-$HOME/repos}"

run -t float "update-repos.sh $REPO_DIR/dot-files/*"
