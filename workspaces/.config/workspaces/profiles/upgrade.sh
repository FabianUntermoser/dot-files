# This script is not supposed to be run as a standalone script.
# It is run from the `workspace.sh` script.

wait_for_network || exit
switch_workspace
append_layout

CLOUD_DIR="${CLOUD_DIR:-$HOME/cloud}"
REPO_DIR="${REPO_DIR:-$HOME/repos}"

[ -d "$CLOUD_DIR/4ARCHIVE/media/music/" ] && {
    run -t "first"  "youtube-dl-playlist.sh $CLOUD_DIR/4ARCHIVE/media/music/yt-chill/ Chill; $SHELL"
    run -t "first"  "youtube-dl-playlist.sh $CLOUD_DIR/4ARCHIVE/media/music/yt-break/ Bboy; $SHELL"
    run -t "first"  "youtube-dl-playlist.sh $CLOUD_DIR/4ARCHIVE/media/music/yt-house/ House; $SHELL"
    run -t "first"  "youtube-dl-playlist.sh $CLOUD_DIR/4ARCHIVE/media/music/yt-majestic/ Majestic; $SHELL"
}
run -t "second" "update-repos.sh -q $REPO_DIR; $SHELL"
# run -t "third"  "nvim +PlugClean! +CocUpdate +PlugUpgrade +PlugInstall +PlugUpdate"
run -t "fourth" "yay -Quq --aur | xargs -n 1 yay -S --noconfirm; $SHELL"
