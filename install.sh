#!/usr/bin/env bash

die() { echo "$@" 1>/dev/stderr; exit 1; }

setup_auto_stow() {
# setup git hook to auto stow rc files
[ -f .git/hooks/post-merge ] || {
    echo "Setup post-merge git hook ..."
    cat <<-EOF > .git/hooks/post-merge
	#!/bin/sh
	./$0
	EOF
    chmod +x .git/hooks/post-merge
}
}

setup_git_commit_helper() {
# setup git hook to prepopulate commit message
[ -f .git/hooks/prepare-commit-msg ] || {
    echo "Setup git commit message hook ..."
    cat <<-'EOF' > .git/hooks/prepare-commit-msg
	#!/bin/sh

	COMMIT_MSG_FILE=$1

	# abort on existing commit source (rebase)
	[ -n "$COMMIT_SOURCE" ] && exit

	staged_file=$(git status --porcelain |
	    sed -e '/^ /d' | # show only staged files
	    head -1 | # limit to first item
	    awk '{ print $2 }') # get filename

	[ -z "$staged_file" ] && exit

	# for top level files, use filename as scope
	# otherwise the most uppertop folder
	staged_dir=$(dirname $staged_file)
	if [ "$staged_dir" = "." ]; then
	    scope=$staged_file
	else
	    scope=${staged_dir%%/*} # top level dir
	fi

	commit_msg_template="feat($scope): "

	# insert commit message template on changes to diary
	sed -i "1 i $commit_msg_template" $COMMIT_MSG_FILE
	EOF
    chmod +x .git/hooks/prepare-commit-msg
}
}

remove_dead_symlinks() {
    echo "Removing dead symlinks ..."

    # identify top level stow directories
    # this limits the amount of searching we have to do
    find * -mindepth 2 -maxdepth 2 -type d | # list directories to stow
        sed 's#^[^/]*/##g' | # remove top level dir name
        # find and remove dead symoblic links in top level stow dir
        xargs -I % find "$HOME/%" -xtype l -exec rm -v {} \;
}

stow_files() {
    # ensure some directories are not symlinked
    mkdir -p "$HOME/.vim/autoload"
    mkdir -p "$HOME/.config/zsh"
    mkdir -p "$HOME/.config/coc"
    mkdir -p "$HOME/.config/xournalpp/autosave/"
    mkdir -p "$HOME/.config/xournalpp/metadata/"
    mkdir -p "$HOME/.config/ranger/plugins/"
    mkdir -p "$HOME/.local/bin"
    mkdir -p "$HOME/.local/share/applications/"
    mkdir -p "$HOME/.config/JetBrains/"

    # prevent generated files from fucking up my stowing
    rm -f "$HOME/.Xresources"
    rm -f "$HOME/.config/mimeapps.list"

    # ensure executable permissions
    find . -type f -name "*.sh" -exec chmod +x {} \;

    # stow dot files
    # shellcheck disable=SC2046,SC2035
    stow -v -t "$HOME" $* --adopt
}

# initialize
setup_auto_stow
setup_git_commit_helper

remove_dead_symlinks

# overwrite packages with program arguments
if [ $# -gt 0 ]; then
    printf "\nStowing dot-files for $@ ...\n"
    stow_files "$@"
else
    printf "\nStowing all dot-files ...\n"
    stow_files $(ls -d */)
fi


# setup custom profile
grep "profile.custom" "$HOME"/.profile || echo 'source ~/.profile.custom' >> "$HOME"/.profile

# ignore future changes for files that get updated externally
list=(
    nvim/.config/nvim/coc-settings.json       # coc settings ignored due to toggling of git gutter icons
    xournalpp/.config/xournalpp/settings.xml  # gets updated with device specific dimension settings
    )
for file in "${list[@]}" ; do
    git update-index --skip-worktree "$file"
done
