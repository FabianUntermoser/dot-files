function init#markdown() abort
    augroup markdown
        autocmd!
        setlocal spell

        " set tab shift size
        setlocal tabstop=2
        setlocal shiftwidth=2
        setlocal softtabstop=2

        " prevent gq to reformat headings
        setlocal comments+=:#

        " hide certain syntax characters
        setlocal conceallevel=2

        " conceal markdown elements
        setlocal filetype=markdown.mkdc

        " continue comment on next line
        set formatoptions+=o

        " insert current time
        imap <buffer> <silent> <A-i> <C-R>=strftime("%H:%M") . " "<CR>
        nmap <buffer> <silent> <A-i> i<C-R>=strftime("%H:%M") . " "<CR>

        " insert wikilink
        imap <buffer> <silent> <A-l> [[

        " insert single codeblock
        imap <buffer> <silent> <A-c> ``<Left>

        " custom markdown syntax
        autocmd BufEnter *.mkdc set ft=markdown.mkdc
        autocmd BufEnter *.md set ft=markdown.mkdc
        autocmd BufEnter *.markdown set ft=markdown.mkdc

    augroup end
endfunction

function init#tex() abort
    augroup tex
        autocmd!
        " clear build files when leaving tex files
        autocmd VimLeave *.tex !clear-tex-build-files.sh %

        " jump to position in pdf
        nmap <silent> gD
                    \ :CocCommand latex.ForwardSearch<CR>

        " break line on maximum character limit
        set textwidth=80

        " enable spell checking
        setlocal spell
    augroup end
endfunction

function init#c() abort
    augroup c
        autocmd!
        " Clang Auto Format on save
        let g:clang_format#auto_format = 0
        " Ignore false makefile error
        setglobal errorformat^=%-Gmake:\ ***%m
        " Run ClangFormat on operator
        noremap <localleader>f <Plug>(operator-clang-format)
        " Toggle Clang Auto Format
        nnoremap <F8> :ClangFormatAutoToggle<CR>
    augroup end
endfunction
