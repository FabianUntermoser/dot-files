function notes#init() abort

    " navigate between neighbouring files with unimpaired.vim
    nmap <buffer> gn ]f
    nmap <buffer> gp [f

    if matchstr(expand('%:p:h'), "notes") == ""
        " echo "notes#init: not in notes directory"
        return
    endif

    map <buffer> <leader>f  :FindNotes<CR>

    " enter goyo for focused note taking
    " somehow doesnt work
    " Goyo 100x80%

    if matchstr(expand('%:p:h'), "daily") == ""
        " echo "notes#init: not in diary directory"
        return
    endif

    augroup diary
        autocmd!

        " diary navigation mappings
        noremap <silent> <localleader>dn :call notes#diary_next()<CR>
        noremap <silent> <localleader>dp :call notes#diary_prev()<CR>

        noremap <silent> gw :call notes#diary_week()<CR>

        " continue diary timestamp table
        " TODO: fix autocomplete not working
        " imap <buffer> <CR> <CR><Esc>:call <SID>auto_diary_table()<CR>A
        " nmap <buffer> o o<Esc>:call <SID>auto_diary_table()<CR>A
        " nmap <buffer> O O<Esc>:call <SID>auto_diary_table()<CR>A

    augroup end
endfunction

" Diary navigation
function notes#diary_week() abort
    let l:filename = expand('%:t:r')
    let l:date = system("date -d" . "'" . l:filename . " +1 week'" . ' +\%Y-W%W')
    execute 'edit ' . expand('%:h') . '/../weekly/' . trim(l:date) . '.md'
endfunction

" Automatically continue/end timestamp markdown table
function! s:auto_diary_table()
  let l:preceding_line = getline(line(".") - 1)

  " match timestamp and separator: "12:34 |"
  let l:line_rgx = '\v^\d{2}:\d{2} \|'

  if l:preceding_line =~ l:line_rgx . ' \w' " line continues
      let l:list_index = matchstr(l:preceding_line, '\v^\d{2}') " first two digits
      call setline(".", l:list_index . ":00 | ")
      " increment hour by one
      normal 
  elseif l:preceding_line =~ l:line_rgx . '\s$'  " line ends
    " end the list and clear the empty item
    call setline(line(".") - 1, "")
  endif
endfunction
