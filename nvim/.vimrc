" vim: set foldmethod=marker foldlevel=0 nomodeline:

" INFO: This is outdated config.
" I currently use the lua configuration for neovim found in ~/.config/nvim/init.lua

" Packer configuration
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

" Plugins {{{

" use enew for Plugin windows
let g:plug_window='enew'

" Auto Install Vim-Plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

"" integration
Plug 'svermeulen/vim-yoink'
Plug 'mhinz/vim-startify'
Plug 'Lokaltog/neoranger'

" fzf
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

"" themes
Plug 'vim-airline/vim-airline'
Plug 'dawikur/base16-vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
Plug 'flazz/vim-colorschemes'
Plug 'dylanaraps/wal.vim'
Plug 'chriskempson/base16-vim'

"" search
Plug 'easymotion/vim-easymotion'
Plug 'haya14busa/incsearch.vim'
Plug 'haya14busa/incsearch-easymotion.vim'
Plug 'romainl/vim-cool' " disable highlighting after searching (auto nohlsearch)

"" version control
Plug 'tpope/vim-fugitive'
Plug 'shumphrey/fugitive-gitlab.vim'

"" text objects
Plug 'michaeljsmith/vim-indent-object'
Plug 'kana/vim-textobj-user'
Plug 'kana/vim-textobj-line'

" autogenerate tag files
" Plug 'ludovicchabant/vim-gutentags' " -> creates huge tag files
Plug 'majutsushi/tagbar'

"" languages
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'leafgarland/typescript-vim', { 'for': 'typescript' }
Plug 'pangloss/vim-javascript', { 'for': ['javascript', 'javascript.jsx', 'html'] }
Plug 'honza/vim-snippets'
Plug 'ekalinin/Dockerfile.vim' " dockerfile syntax
Plug 'lervag/vimtex', { 'for': 'tex' }

" c
Plug 'rhysd/vim-clang-format'
Plug 'Shougo/vimproc.vim', {'do' : 'make'} " requirement of vim-clang-format
Plug 'kana/vim-operator-user' " requirement of vim-clang-format

" javascript
Plug 'prettier/vim-prettier' " TODO: is this still used? replace with treesitter perhaps

"" UI/UX
Plug 'junegunn/goyo.vim' " focused writing
Plug 'junegunn/limelight.vim' " highlight current paragraph

"" utilities
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-characterize' " diagraphs
Plug 'tpope/vim-speeddating' " mutate dates
Plug 'junegunn/vim-easy-align' " align paragraph
Plug 'editorconfig/editorconfig-vim' " support editorconfig files
Plug 'AndrewRadev/switch.vim'
Plug 'jez/vim-superman' " man pages
Plug 'rstacruz/sparkup' " fast html writing
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' } " color highlighting preview
Plug 'gelguy/wilder.nvim' " wildmenu with fuzzy find
Plug 'voldikss/vim-translator' " online translator

call plug#end()
" }}}

" Vi Settings {{{
if !has('nvim')
    if &cp | set nocp | endif
    let s:cpo_save=&cpo
    set cpo&vim
    let &cpo=s:cpo_save
    unlet s:cpo_save
    set autoindent
    set backspace=indent,eol,start
    set backup
    set backupdir=~/.cache/vim/backup//
    set directory=~/.cache/vim/swap//
    set undodir=~/.cache/vim/undo//
    set fileencodings=ucs-bom,utf-8,default,latin1
    set helplang=en
    set history=1000
    set hlsearch
    set ruler
    set showcmd
    set wildmenu
    set autoread
    set modelines=0
    set encoding=utf-8
    set ttyfast
    set ttymouse=sgr

    vmap gx <Plug>NetrwBrowseXVis
    nmap gx <Plug>NetrwBrowseX
    vnoremap <silent> <Plug>NetrwBrowseXVis :call netrw#BrowseXVis()
    nnoremap <silent> <Plug>NetrwBrowseX :call netrw#BrowseX(expand((exists("g:netrw_gx")? g:netrw_gx : '<cfile>')),netrw#CheckIfRemote())

endif
" }}}

" Settings {{{
filetype plugin indent on
syntax on
set undofile
set noswapfile
set ignorecase
set smartcase
set smartindent
set hidden
set showmatch
set linebreak
set number
set relativenumber
set scrolloff=5
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set mouse=a
set showbreak=›
set splitbelow splitright
set nojoinspaces
set switchbuf=usetab
set virtualedit=block
set noemoji
" set spelllang=en,de
set nofixendofline

" yank/paste into/from system clipboard
set clipboard+=unnamedplus

" continue comment on next line
set formatoptions+=ro

" folding
set nofoldenable
set foldmethod=indent
set foldlevel=0

" highlight line cursor is on
set cursorline

" highlight whitespace characters
set list listchars=nbsp:¬,tab:»·,trail:·,extends:>

if has('nvim')
    " show command previews (substitute)
    set inccommand=nosplit
endif

" indent line on linebreaks
if has('linebreak')
    set breakindent
    if exists('&breakindentopt')
        set breakindentopt=shift:2
    endif
endif

" enable synonym completion with i_CTRL-X_CTRL-T
set thesaurus=~/.vim/thesaurus/mthesaur.txt

" set leader key to space
let mapleader = " "
let maplocalleader="\<"
" }}}

" Plugin Configuration {{{

" Plugin Vim-Plug
autocmd FileType vim-plug setlocal nofoldenable

" Plugin Vim-Startiy {{{
let g:startify_commands = [
            \   { 'up': [ 'Update Plugins', ':PlugUpdate' ] },
            \   { 'urp': [ 'Update Plugins', ':UpdateRemotePlugins' ] },
            \   { 'ug': [ 'Upgrade Plugin Manager', ':PlugUpgrade' ] },
            \   { 'uc': [ 'Update COC', ':CocUpdate' ] },
            \   { 'd' : [ 'Open Diary', ':OpenDiary' ] },
            \ ]
let g:startify_bookmarks = [
            \ { 'w': '$NOTES_DIR/index.md' },
            \ { 's': '$NOTES_DIR/scratchpad.md' },
            \ { 'v': '~/.vimrc' },
            \ { 'z': '~/.config/zsh/.zshrc' }
            \ ]
" }}}

" Plugin FZF {{{
" Fzf Layout Settings
let g:fzf_layout = {'down': '40%'}

" fzf action: build quickfix list
function! s:fzf_build_quickfix_list(lines)
  call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
  copen
  cc
endfunction

" fzf action: vimdiff file
function! s:fzf_vdiffsplit(lines)
  execute "Gvdiffsplit " . join(a:lines)
endfunction

let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-s': 'split',
  \ 'ctrl-v': 'vsplit',
  \ 'ctrl-q': function('s:fzf_build_quickfix_list'),
  \ 'ctrl-d': function('s:fzf_vdiffsplit'),
  \ }

" mappings
map <silent> <leader>b  :Buffers<CR>
map <silent> <leader>t  :Tags<CR>
map <silent> <leader>h  :Helptags<CR>
map <silent> <leader>f  :Files<CR>
map <silent> <leader>F  :FindAllFiles<CR>
map <silent> <leader>a  :Rg!<CR>
vmap <silent> <leader>a :RgVisual!<CR>
map <silent> <leader>l  :Lines<CR>
map <silent> <leader>gg :Glcd<CR>
" show git status files
map <silent> <leader>gS :GFiles?<CR>
" show all files in repository
map <silent> <leader>gf :GFiles --cached --exclude-standard --recurse-submodules<CR>
map <silent> <leader>r  :History:<CR>
map <silent> <leader>R  :ShellHistory<CR>
map <silent> <leader>C  :Commands<CR>
map <silent> <leader>m  :Maps<CR>

" complete file path
imap <expr> <c-x><c-f> fzf#vim#complete#path('rg --files --follow --hidden --no-ignore ')

" complete command from shell history
command! -nargs=0 ShellHistory call feedkeys("i<c-x><c-R>", 't')
inoremap <expr> <c-x><c-R>
            \ fzf#vim#complete(fzf#wrap({
            \ 'source': "sed -e 's/: [0-9]*:0;//' -e '/^$/d' $HISTFILE" ,
            \ 'options': '--tac --no-sort',
            \ }))

" complete git commits
inoremap <expr> <c-x><c-g>
            \ fzf#vim#complete(fzf#wrap({
            \ 'source': "git log --pretty=format:%s"
            \ }))

" find all (including ignored) files
" uses rg with the `--no-ignore` option, and `find` as a fallback
command! -nargs=0 -bang FindAllFiles
            \ call fzf#run(fzf#wrap({
            \   'source': 'rg --files --follow --hidden --no-ignore || '
            \                .  'find . -path ./.git -prune -o -print'
            \ }, <bang>0))

command! -nargs=0 -bang FindNotes
            \ call fzf#run(fzf#wrap({
            \   'source': 'rg --files --follow --type md || '
            \ }, <bang>0))

" get word from last visual selection
" taken from: https://stackoverflow.com/questions/1533565/how-to-get-visually-selected-text-in-vimscript
function! s:get_visual_selection()
    return getline("'<")[getpos("'<")[2]-1:getpos("'>")[2]-1]
endfunction

" Fuzzy Find inside Files with RipGrep
command! -nargs=* -bang Rg call RipgrepFzf(<q-args>, <bang>0)
command! -bang -range RgVisual call RipgrepFzf(s:get_visual_selection(), <bang>0)

function! RipgrepFzf(query, fullscreen)
    let command_fmt = 'rg --hidden --column --line-number --no-heading --color=always --smart-case %s || true'
    let command_fmt_all = 'rg --no-ignore --hidden --column --line-number --no-heading --color=always --smart-case %s || true'
    let query = a:query
    let initial_command = printf(command_fmt, shellescape(query))
    let f1_command = printf(command_fmt, '{q}')
    let f2_command = printf(command_fmt_all, '{q}')
    let spec = { 'options': [
                \ '--phony',
                \ '--query', query,
                \ '--bind', 'change:reload:'.f1_command,
                \ '--bind', 'f2:reload('.f2_command.')',
                \ '--preview-window', (a:fullscreen == v:null ? 'right' : 'up') . ':nohidden'
                \ ]}
    call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
endfunction
" }}}

" Plugin Vim-Airline {{{
" display all buffers if only one tab is open
let g:airline#extensions#tabline#enabled = 1
" }}}

" Plugin EasyAlign{{{
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
" }}}

" Plugin IncSearch{{{
" use easymotion while searching
" map / <Plug>(incsearch-easymotion-/)
" map ? <Plug>(incsearch-easymotion-?)
" }}}

" Plugin YankRing{{{
let g:yankring_history_dir = '~/.vim'
" }}}

" Plugin easysearch {{{
" search for motion
map <leader>s <Plug>(easymotion-prefix)
" }}}

" Plugin Tagbar {{{
map <silent> <leader>T :TagbarToggle <CR>
" }}}

" Plugin Limelight {{{
let g:limelight_default_coefficient = 0.7
nmap <leader>L :Limelight!!<CR>
" }}}

" Plugin GOYO {{{
nmap <leader>G :Goyo<CR>

let g:goyo_width=100
let g:goyo_height='80%'

function! s:goyo_enter()
    set noshowmode
    set noshowcmd
    :CocCommand git.toggleGutters
endfunction

function! s:goyo_leave()
    set showmode
    set showcmd
    :CocCommand git.toggleGutters
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

" }}}

" Plugin Vim-Hexokinase {{{
let g:Hexokinase_highlighters = ['backgroundfull']
" }}}

" Plugin Vim-Fugitive {{{
nmap <silent> <leader>gs :Git<CR>)
nmap <silent> <leader>gr :Gread<CR>
nmap <silent> <leader>gw :Gwrite<CR> :Gstatus<CR>
nmap <silent> <leader>gc :Gwrite<CR> :Git commit -v<CR>
nmap <silent> <leader>gl :Gclog<CR>
nmap <silent> <leader>ge :Git edit<CR>
nmap <silent> <leader>gd :Git difftool<CR>
nmap <silent> <leader>gD :Git difftool -y<CR>
nmap <silent> <leader>gm :Git mergetool<CR>
nmap <silent> <leader>gb :Git blame<CR>
nmap <silent> <leader>gF :Git fetch<CR>
nmap <silent> <leader>gL :Git pull<CR>
nmap <silent> <leader>gP :Git push<CR>
nmap <silent> <leader>gB :GBrowse<CR>

" show git status window on startup if repo is dirty
silent :!test -n "$(git status --porcelain)"
if !v:shell_error
    autocmd User StartifyReady ++nested :Git
endif
" }}}

" autoclose fugitive buffers
autocmd BufReadPost fugitive://* set bufhidden=delete

" quick rebasing from status window
autocmd FileType fugitive nmap <buffer> ri :G rebase -i HEAD~

" push to gerrit
command! -nargs=0 Gpg    :Git push --progress origin refs/heads/master:refs/for/master

" navigate between chunks in status window
autocmd FileType fugitive nmap <buffer> <localleader>gn ]c
autocmd FileType fugitive nmap <buffer> <localleader>gp [c

" override mappings in fugitive windows
autocmd User FugitiveBlob,FugitiveStageBlob call s:BlobOverrides()
function s:BlobOverrides()
    " navigate between chunks in diff mode
    nmap <buffer> <localleader>gn ]c
    nmap <buffer> <localleader>gp [c
    " disable mappings to avoid breaking diff screen
    nmap <buffer> <S-l> <nop>
    nmap <buffer> <S-h> <nop>
endfunction

" }}}

" Plugin Surround{{{
let g:surround_no_mappings = 1
nmap ds  <Plug>Dsurround
nmap cs  <Plug>Csurround
nmap cS  <Plug>CSurround
nmap ys  <Plug>Ysurround
nmap yS  <Plug>YSurround
nmap yss <Plug>Yssurround
nmap ySs <Plug>YSsurround
nmap ySS <Plug>YSsurround
" }}}

" Plugin COC {{{
" proprietary: coc-phpls
let g:coc_global_extensions = [
  \ 'coc-git',
  \ 'coc-snippets',
  \ 'coc-html',
  \ 'coc-css',
  \ 'coc-sh',
  \ 'coc-java',
  \ 'coc-java-debug',
  \ 'coc-clangd',
  \ 'coc-tsserver',
  \ 'coc-prettier',
  \ 'coc-phpls',
  \ 'coc-calc',
  \ 'coc-pairs',
  \ 'coc-docker',
  \ 'coc-vimlsp',
  \ 'coc-angular',
  \ 'coc-pyright',
  \ 'coc-yaml',
  \ 'coc-vimtex'
  \ ]

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1):
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Navigate errors/diagnostics
nmap <silent> <localleader>en <Plug>(coc-diagnostic-next)
nmap <silent> <localleader>ep <Plug>(coc-diagnostic-prev)

" Remap keys for gotos
nmap <silent> gD <Plug>(coc-definition)
nmap <silent> gT <Plug>(coc-type-definition)
nmap <silent> gI <Plug>(coc-implementation)
nmap <silent> gR <Plug>(coc-references)

"" coc-git
nmap <localleader>gn <Plug>(coc-git-nextchunk)
nmap <localleader>gp <Plug>(coc-git-prevchunk)
nmap <localleader>gu :CocCommand git.chunkUndo<cr>
nmap <localleader>gi :CocCommand git.chunkInfo<cr>

" Show documentation in preview window
nnoremap <silent> <localleader><space> :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

nmap <leader>cr :CocRestart<CR>
nmap <leader>ch <Plug>(coc-float-hide)

" Remap for rename current word
nmap <localleader>rn <Plug>(coc-rename)
nmap <localleader>rf <Plug>(coc-refactor)

" Remap for format selected region
xmap <localleader>f  <Plug>(coc-format-selected)
nmap <localleader>f  <Plug>(coc-format-selected)

" Remap for do codeAction of selected region, <leader>aap for current paragraph
xmap <localleader>a  <Plug>(coc-codeaction-selected)
nmap <localleader>a  <Plug>(coc-codeaction-selected)

" Code Action for the current line (i.e current class)
nmap <localleader>A  <Plug>(coc-codeaction-selected)l

" Fix autofix problem of current line
nmap <localleader>F  <Plug>(coc-fix-current)

" Create mappings for function text object, requires document symbols feature of languageserver.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

"" coc-snippets
" use TAB for snippet jumping as well as selection confirm
let g:coc_snippet_next = '<tab>'
let g:coc_snippet_prev = '<s-tab>'

" Update signature help on jump placeholder
autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')

" Use `:Format` to format current buffer
command! -nargs=0 Format    :silent call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold      :silent call CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR        :silent call CocAction('runCommand', 'editor.action.organizeImport')

" Using CocList
nnoremap <silent> <leader>cl  :<C-u>CocList<cr>
" Show all errors/diagnostics
nnoremap <silent> <leader>ce  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <leader>cx  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <leader>cc  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <leader>co  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <leader>cs  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <leader>cj  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <leader>ck  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <leader>cp  :<C-u>CocListResume<CR>
" }}}

" Plugin neoranger {{{
nnoremap <silent> - :RangerCurrentFile<CR>
" }}}

" Plugin switch.vim {{{
" Switch '[ ]' to '[x]' and back to '[ ]'
autocmd FileType markdown let b:switch_custom_definitions =
    \ [
    \   { '\v^(\s*[*+-] )?\[ \] (.*)': '\1[x] \2',
    \     '\v^(\s*[*+-] )?\[x\] (.*)': '\1[-] \2',
    \     '\v^(\s*[*+-] )?\[-\] (.*)': '\1[ ] \2',
    \   },
    \   { '\v^(\s*\d+\. )?\[ \] (.*)': '\1[x] \2',
    \     '\v^(\s*\d+\. )?\[x\] (.*)': '\1[-] \2',
    \     '\v^(\s*\d+\. )?\[-\] (.*)': '\1[ ] \2',
    \   },
    \ ]
" }}}

" {{{ Plugin wilder.nvim
call wilder#setup({'modes': [':']})

call wilder#set_option('pipeline', [
  \   wilder#branch(
  \     wilder#python_file_finder_pipeline({
  \       'file_command': ['rg', '--files', '--hidden'],
  \     }),
  \     wilder#cmdline_pipeline({
  \       'fuzzy': 1,
  \     }),
  \   ),
  \ ])

call wilder#set_option('renderer', wilder#popupmenu_renderer({
  \ 'highlighter': wilder#basic_highlighter(),
  \ 'highlights': {
  \   'accent': wilder#make_hl('WilderAccent', 'Pmenu', [{}, {}, {'foreground': '#f4468f'}]),
  \ },
  \ 'left': [ wilder#popupmenu_devicons() ],
  \ 'right': [ wilder#popupmenu_scrollbar() ],
  \ }))
" }}}

" {{{ Plugin vim-yoink
nmap <c-n> <plug>(YoinkPostPasteSwapBack)
nmap <c-p> <plug>(YoinkPostPasteSwapForward)
nmap p <plug>(YoinkPaste_p)
nmap P <plug>(YoinkPaste_P)
" }}}


" {{{ Plugin voldikss/vim-translator
let g:translator_target_lang = "en"

" Echo translation in the cmdline
nmap <silent> <Leader>t <Plug>Translate
vmap <silent> <Leader>t <Plug>TranslateV
" Display translation in a window
nmap <silent> <Leader>tw <Plug>TranslateW
vmap <silent> <Leader>tw <Plug>TranslateWV
" Replace the text with translation
nmap <silent> <Leader>tr <Plug>TranslateR
vmap <silent> <Leader>tr <Plug>TranslateRV
" Translate the text in clipboard
nmap <silent> <Leader>tx <Plug>TranslateX
" }}}

" }}}

" Colors {{{
colorscheme base16-materia
let g:airline_theme='base16_solarized_dark'

" enable true color support
set termguicolors
" set Vim-specific sequences for RGB colors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

function HighlightTerminalCursor() abort
    " highlight groups for terminal cursor
    hi! TermCursorNC ctermfg=15 guifg=#fdf6e3 ctermbg=14 guibg=#93a1a1 cterm=NONE gui=NONE
endfunction

function ToggleTheme() abort
    if &background ==? "light"
        Dark
    else
        Light
    endif
    call HighlightTerminalCursor()
endfunction

function UpdateTheme() abort
    " base16 theme setup
    if filereadable(expand("~/.vimrc_background"))
        let base16colorspace=256
        let theme = getenv('BASE16_THEME')
        if theme != v:null
            let g:airline_theme='base16_' . substitute(theme, "-", "_", "g")
        endif
        source ~/.vimrc_background
    endif
    call HighlightTerminalCursor()
endfunction

command! -nargs=0 ToggleTheme  call ToggleTheme()
command! -nargs=0 UpdateTheme  call UpdateTheme()
command! -nargs=0 Light        :AirlineTheme base16_solarized_light | colorscheme base16-atelier-savanna-light | set background=light
command! -nargs=0 Dark         :AirlineTheme base16_solarized_dark  | colorscheme base16-solarized-dark  | set background=dark
command! -nargs=0 Wal          :AirlineTheme wal                    | colorscheme wal

call UpdateTheme()
" }}}

" Terminal {{{
if has('nvim')
    " launch terminal in insert mode
    au TermOpen * startinsert

    " window navigation in terminal mode, also enters normal mode
    tmap <silent> <C-w>k <C-\><C-n>:wincmd k<CR>
    tmap <silent> <C-w>j <C-\><C-n>:wincmd j<CR>
    tmap <silent> <C-w>h <C-\><C-n>:wincmd h<CR>
    tmap <silent> <C-w>l <C-\><C-n>:wincmd l<CR>

    " convenvience bindings for window navigation
    tmap <silent> <C-k> <C-w>k
    tmap <silent> <C-j> <C-w>j
    tmap <silent> <C-h> <C-w>h
    tmap <silent> <C-l> <C-w>l

    " pane resizeing in terminal mode
    tmap <silent> <C-w>+ <C-\><C-n>:resize +2<CR>i
    tmap <silent> <C-w>- <C-\><C-n>:resize -2<CR>i

    " simulate |i_CTRL-R| in terminal-mode:
    tnoremap <expr> <C-R> '<C-\><C-N>"'.nr2char(getchar()).'pi'
endif
" }}}

" Mappings {{{
" Easier : commands
noremap , :

" yank to end of line
nnoremap Y y$

" yank filepath
nnoremap y. :let @+ = expand("%")<cr>

" makes dj delete two lines instead of only one
nnoremap dj dj
nnoremap dk dk

" yank file path
nnoremap <Leader>y :let @+=expand("%:~:.")<CR>:echo 'Yanked relative path'<CR>
nnoremap <Leader>Y :let @+=expand("%:p")<CR>:echo 'Yanked absolute path'<CR>

" Allow j/k for breaked lines
map j gj
map k gk

" move to start/end of line
map gh g^
map gl g$

" Avoid unintentional switches to Ex mode
nmap Q q

" indent/unindent current line
" NOTE: breaks <C-i>
" imap <TAB> <esc>>>3li
" imap <S-Tab> <esc><<i

" Ctrl-c for copying to clipboard + primary selection!
vnoremap <C-c> "+y

" jump to tag
nmap gt <C-]>zz

" keep visual selection when indenting/outdenting
vmap < <gv
vmap > >gv

" replace/substitute
nnoremap R :% s##gc<Left><Left><Left>
" replace/substitute visual selection
vnoremap R y :% s#<C-R>"##gc<Left><Left><Left>

" search mappings
nnoremap s /
vnoremap s y/<C-R>"<CR>
vnoremap S y/<C-R>"
nnoremap S y/<C-R>"
nnoremap qs q:?

" {{{ spelling mappings
nmap <localleader>S :set spell! <CR>
nmap <localleader>sn ]s
nmap <localleader>sp [s
nmap <localleader>sf z=
nmap <localleader>sg zg
nmap <localleader>sb zb

" fix last spelling mistake for whole file
nnoremap <silent> <Plug>FixLastSpellingMistake
            \ [s1z= :spellr<CR>
            \ :call repeat#set("\<Plug>FixLastSpellingMistake")<CR> " makes command repeatable
nmap <localleader>sF <Plug>FixLastSpellingMistake

" fix last spelling mistake for whole file in insert mode and jump back to last insert position
" resetting the undo level ends the current undo block, allowing to rapid fire
" this command while being able to undo the changes individually
inoremap <C-F> <Esc>[s 1z= :silent! spellr<CR> :let &undolevels=&undolevels<CR> gi

" suggest spelling correcting for mistake
inoremap <C-L> <Esc>[sea<C-x><C-s>
" }}}

" create new file from location of buffer
nmap <localleader>n :edit <C-R>=expand('%:p:h') . '/'<CR>

" remove current file
nmap <localleader>d :call delete(expand('%')) <bar> bdelete!
nmap <localleader>D :call delete(expand('%')) <bar> quit!

" dragon-drop current file
nmap <silent> <localleader>dr :w!<CR> :!GDK_BACKEND=x11 dragon-drop --and-exit %<CR>

" easy search & replace (repeatable with dot)
nnoremap c* *Ncgn

" make text italic/bold
vmap <silent> <C-i> c*<C-o>P<C-o>l*<esc>
vmap <silent> <C-b> c**<C-o>P<C-o>l**<esc>

" let %% expand to path of currently open file in command mode
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'

" command mode moving
cnoremap <C-a> <Home>
cnoremap <C-e> <End>

" Map alt-v in command-line mode to replace the commandline with the Ex command-line beneath the cursor in the buffer
cnoremap <M-v> <C-\>esubstitute(getline('.'), '^\s*\(' . escape(substitute(&commentstring, '%s.*$', '', ''), '*') . '\)*\s*:*' , '', '')<CR>

" dot files management
map <silent> <leader>ev :edit `=resolve(expand("~/.vimrc"))`<CR>
map <silent> <leader>ez :edit `=resolve(expand("~/.config/zsh/.zshrc"))`<CR>
map <silent> <leader>ei :edit `=resolve(expand("~/.config/i3/config"))`<CR>
map <silent> <leader>ex :edit ~/.Xresources <CR>
map <silent> <leader>sv :w \| source ~/.vimrc <CR>

" save/quit quickly
nmap <silent> <leader>w :write!<CR>
nmap <silent> <leader>W :wall!<CR>
nmap <silent> <leader>q :quit<CR>
nmap <silent> <leader>Q :quit!<CR>
nmap <silent> <leader>d :cclose<CR> :bd!<CR>
nmap <silent> <leader>D :cclose<CR> :bd!<CR>
nmap <silent> <leader>o :only<CR>
nmap <silent> <leader>O :CloseOtherBuffers<CR>
nmap <silent> <leader>E :edit!<CR>

" write to
cnoremap w!! execute 'silent! write !SUDO_ASKPASS=`which ssh-askpass` sudo tee % >/dev/null' <bar> edit!

" quickfix navigation
nmap <localleader>cn :cn <CR>
nmap <localleader>cp :cp <CR>
nmap <localleader>co :copen <CR>

" window navigation
nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-l> :wincmd l<CR>

" buffer navigation
nnoremap <esc><esc> <C-^>
nmap <silent> <A-k> :bn<CR>
nmap <silent> <A-j> :bp<CR>

" allow indenting if <localeader> is <<
nnoremap <localleader><localleader> <<

" method navigation
nnoremap mn ]m
nnoremap mp [m

" window splits
nmap <silent> <C-Right> :wincmd v<CR>
nmap <silent> <C-Down> :wincmd s<CR>

" folds
" nmap <leader><leader> zA
nmap <leader><leader> za
nmap <leader>n zj
nmap <leader>p zk

" launch terminal
map <Leader>- :new <bar> resize 10 <bar> term<CR>

" open file with default application
nnoremap gF :!xdg-open <cfile> &<CR><CR>

" display unprintable characters
map <F1> :set list!<CR>

" enable folding
map <F2> :set foldenable!<CR>

" disable automatic changes to current buffer
map <F3> :autocmd! autofixes<CR>

" run default vim make
map <F4> :wa!<CR> :make<CR>

" run make in nvim terminal
map <F5> :silent! wa!<CR> :terminal make<CR>

" search command history for make
map <F6> q:?\v(terminal make\|make)<space><CR>

map <F7> :wa!<CR> :terminal valgrind -s --leak-check=full make<CR>

map <F8> :ToggleTheme<CR>

" }}}

" Commands {{{
command! -nargs=0 CloseOtherBuffers :%bdelete!|edit #|.1,$bdelete
command! -nargs=0 AutoSave autocmd TextChanged * update

command! -nargs=0 OpenDiary call OpenDiary()
function OpenDiary() abort
    let l:date = system('date +\%F')
    execute 'cd $NOTES_DIR'
    execute 'edit $DIARY_DIR/' . trim(l:date) . '.md'
endfunction

" Open digital note online
" xdg-open "obsidian://open?vault=notes&file=edu%2ffh-sbg%2fMMT1%2fADB%2f03-ADB.md"
nmap <silent> <leader>gO :OpenInObsidian<CR>
command! -nargs=0 OpenInObsidian call jobstart(["setsid","-f", "xdg-open", "obsidian://open?file=" . expand('%:r')])

command! -nargs=0 OpenInBlog call s:OpenInBlog(expand('%<'))

function s:OpenInBlog(filename)
    let note = fnamemodify(a:filename, ":t")
    let note = substitute(note, ' ', '-', 'g')
    call jobstart(["setsid","-f", "xdg-open", "https://ederbit.xyz/notes/" . note])
endfunction
" }}}

" Autocommands {{{

" Automatically open, but do not go to (if there are errors) the quickfix /
" location list window, or close it when is has become empty.
" notes:
" when using, the vimtex plugin, this results in a jump to the error list
autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow

" Equalize window dimensions when resizing vim window
autocmd VimResized * tabdo wincmd =

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
autocmd BufReadPost *
            \ if &ft !~# 'commit' && ! &diff &&
            \      line("'\"") >= 1 && line("'\"") <= line("$")
            \|   execute 'normal! g`"zvzz'
            \| endif

" Detect file changes outside of nvim
autocmd FocusGained * checktime

" }}}

" Filetype specific configuration {{{
autocmd FileType md,markdown call init#markdown()
autocmd FileType md,markdown call notes#init()
autocmd BufEnter md,markdown call notes#init()
autocmd FileType tex         call init#tex()
autocmd FileType c,cpp,objc  call init#c()
" }}}

" Set commentstrings {{{
autocmd FileType vim         setlocal formatoptions-=o
autocmd FileType gitcommit   setlocal spell
autocmd FileType xdefaults   setlocal commentstring=!\ %s
autocmd FileType taskrc      setlocal commentstring=#\ %s
autocmd FileType tridactyl   setlocal commentstring=\"\ %s
autocmd FileType matlab      setlocal commentstring=%\ %s
autocmd FileType php         setlocal commentstring=<!--%s-->
autocmd FileType apache      setlocal commentstring=#\ %s
" }}}

" Preview files {{{
" open file with application
autocmd FileType *
            \ nmap <localleader>p :w <bar> ! xdg-open % & disown<CR><CR>
autocmd FileType latex,tex,md,markdown
            \ nmap <localleader>p :w <bar> ! $(xdg-open %<.pdf \|\| xdg-open %) &<CR><CR>
autocmd FileType html
            \ nmap <localleader>p :w <bar> ! firefox -new-window %<CR><CR>

" preview file with glow
autocmd FileType md,markdown,adoc
            \ nmap <localleader>p :PreviewGlow<CR>
command! -nargs=0 PreviewGlow
            \ :w <bar> Goyo <bar> Limelight! <bar> execute "terminal glow -p " . expand('%')
" }}}

" Detect Markdown filetype from codeblock {{{
autocmd FileType md,markdown DetectFiletypeFromMarkdownCodeBlock
command! -nargs=0 DetectFiletypeFromMarkdownCodeBlock :call DetectFiletypeFromMarkdownCodeBlock()
function! DetectFiletypeFromMarkdownCodeBlock() abort
    let ft = &ft

    " append first markdown codeblock
    let type = strcharpart(getline(search('^```', 'nw')), 3)
    if type == "c#" | let type = "cs" | endif
    if !empty(type)
        let ft = ft . "." . type
    endif

    " check for latex string
    let match = strcharpart(getline(search('\$', 'nw')), 3)
    if !empty(match)
        let ft = ft . ".latex"
    endif

    execute "setlocal ft=" . ft
endfunction
" }}}

" Automatic fixes for files {{{
command! EnableAutofixes :call Enable_Autofixes()
function! Enable_Autofixes() abort
    augroup autofixes
        autocmd!
        " Remove trailing whitespace on save
        autocmd autofixes BufWritePre * %s/\s\+$//e
    augroup END
endfunction
call Enable_Autofixes()
" }}}

" Auto add files to Fasd index {{{
if has('nvim')
    " automatically add opened files to 'fasd' index
    function! s:fasd_update() abort
        if empty(&buftype) || &filetype ==# 'dirvish'
            call jobstart(['fasd', '-A', expand('%')])
        endif
    endfunction
    augroup fasd
        autocmd!
        autocmd BufWinEnter,BufFilePost * call s:fasd_update()
    augroup END
endif
" }}}

" Compile File & Selection {{{

" compile file based on extension while passing arguments
nnoremap <localleader>cF :CompileFile <C-R>=expand("%")<cr><space>
nnoremap <localleader>cf :CompileFile <C-R>=expand("%")<cr><cr>

" compile visual selection with or without arguments
nnoremap <localleader>cS :CompileSelection<space>
nnoremap <localleader>cs :CompileSelection<CR>
vnoremap <localleader>cS :CompileSelection<space>
vnoremap <localleader>cs :CompileSelection<CR>

" select most recent compile command from history
nnoremap <localleader>cc q:?\vCompile(File\|Selection)<space><cr>
" run most recent compile command from history
nnoremap <localleader>cC q:?\vCompile(File\|Selection)<space><cr><cr>

" compile file
command! -nargs=* -complete=file CompileFile :call s:compile_file(<f-args>)
function s:compile_file(file, ...) abort
    execute "write!"
    " use integrated terminal in nvim
    execute has('nvim') ?
                \ "terminal" : "!clear &&"
                \ "compile.sh" expand(a:file) join(a:000)
endfunction

" run selection in repl
command! -nargs=* -range CompileSelection :call s:compile_selection(<f-args>)
function s:compile_selection(...) abort
    if getline(1) !~ "^#!" | throw "shebang required" | endif
    let l:tmp_file = expand('%') . "-temp"
    " write shebang to repl file
    execute "0" "write!" expand(l:tmp_file)
    " append selection to repl file
    execute "'<,'>" "write!" ">>" expand(l:tmp_file)
    " execute repl file and rm temp file
    execute has('nvim') ? "terminal" : "!clear &&"
                \ "compile.sh" expand(l:tmp_file) join(a:000) "&& rm" expand(l:tmp_file)
endfunction

" }}}
