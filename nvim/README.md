# Vim Configuration

Nvim configuration:
- [.config/nvim/init.lua](.config/nvim/init.lua)

Vim configuration:
- [.config/.vimrc](.config/.vimrc)
- [.config/coc/](.config/coc)
