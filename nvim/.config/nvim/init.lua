-- vim: set foldmethod=marker foldlevel=0 nomodeline:

-- PLUGIN MANAGER {{{
-- Set <space> as the leader key
vim.g.mapleader = ' '
vim.g.maplocalleader = '<'

-- Install package manager
--    https://github.com/folke/lazy.nvim
--    `:help lazy.nvim.txt` for more info
local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)
-- }}}

-- PLUGINS {{{
require('lazy').setup({
  -- Git related plugins
  'tpope/vim-fugitive',
  'tpope/vim-rhubarb',                   -- github
  'cedarbaum/fugitive-azure-devops.vim', -- azure
  'shumphrey/fugitive-gitlab.vim',       -- gitlab

  -- Basic
  'tpope/vim-unimpaired',    -- i use [f and ]f to go to the next/previous file for markdown files
  'tpope/vim-sleuth',        -- Detect tabstop and shiftwidth automatically
  'romainl/vim-cool',        -- Disable highlighting after searching (auto nohlsearch)
  'svermeulen/vim-yoink',    -- yank history
  'tpope/vim-surround',
  'tpope/vim-commentary',
  'tpope/vim-repeat',
  'tpope/vim-characterize',  -- ga over character to print ascii/digraphs info
  'tpope/vim-speeddating',   -- mutate dates
  'junegunn/vim-easy-align', -- align paragraph
  'AndrewRadev/switch.vim',  -- toggle tasks
  'jez/vim-superman',        -- man pages
  'jbyuki/quickmath.nvim',   -- calculator
  'folke/which-key.nvim',    -- show pending keybinds
  'junegunn/limelight.vim',  -- highlight current paragraph

  -- Basic
  'flazz/vim-colorschemes',  -- colorschemes
  'RRethy/base16-nvim',      -- base16 colorschemes
  'dylanaraps/wal.vim',

  -- wildmenu with fuzzy find
  { 'gelguy/wilder.nvim', opts = { modes = { ':' } } },

  -- Languages
  'ElmCast/elm-vim',

  {
    'windwp/nvim-autopairs',
    event = "InsertEnter",
    opts = {} -- this is equalent to setup({}) function
  },

  {
    -- LSP: Mason & lspconfig
    'neovim/nvim-lspconfig',
    dependencies = {
      -- Automatically install LSPs to stdpath for neovim
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',

      -- Useful status updates for LSP
      -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
      { 'j-hui/fidget.nvim', tag = 'legacy', opts = {} },

      -- Additional lua configuration, makes nvim stuff amazing!
      'folke/neodev.nvim',
    },
  },

  {
    -- Autocompletion
    'hrsh7th/nvim-cmp',
    dependencies = {
      -- buffer source
      'hrsh7th/cmp-buffer',

      -- file completion source
      'hrsh7th/cmp-path',

      -- calculator
      'hrsh7th/cmp-calc',

      -- Snippet Engine & its associated nvim-cmp source
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip',

      -- Adds LSP completion capabilities
      'hrsh7th/cmp-nvim-lsp',

      -- Adds a number of user-friendly snippets
      'rafamadriz/friendly-snippets',
    },
  },

  -- AI Code completion
  {
    'zbirenbaum/copilot.lua',
    cmd = "Copilot",
    event = { "InsertEnter", "LspAttach" },
    module = "copilot",
    config = function()
      require("copilot").setup({})

      -- disable copilot by default on startup
      vim.cmd(":Copilot disable")
    end,
  },
  -- Copilot as CMP source
  {
    'zbirenbaum/copilot-cmp',
    config = function()
      require("copilot_cmp").setup()
    end
  },

  {
    -- Adds git related signs to the gutter, as well as utilities for managing changes
    'lewis6991/gitsigns.nvim',
    opts = {
      -- See `:help gitsigns.txt`
      signs = {
        add = { text = '+' },
        change = { text = '~' },
        delete = { text = '_' },
        topdelete = { text = '‾' },
        changedelete = { text = '~' },
      },
      on_attach = function(bufnr)
        local function map(mode, l, r, opts)
          opts = opts or {}
          opts.buffer = bufnr
          vim.keymap.set(mode, l, r, opts)
        end

        map('n', '<localleader>gi', require('gitsigns').preview_hunk, { desc = 'Preview git hunk' })
        map('n', '<localleader>gu', require('gitsigns').reset_hunk, { desc = 'Reset git hunk' })
        map('n', '<localleader>gs', require('gitsigns').stage_hunk, { desc = 'Stage git hunk' })
        map('n', '<localleader>gc', function()
          require('gitsigns').stage_hunk()
          vim.cmd('Git commit -v')
        end, { buffer = bufnr, desc = 'Commit git hunk' })

        -- don't override the built-in and fugitive keymaps
        local gs = package.loaded.gitsigns
        map({ 'n', 'v' }, '<localleader>gn', function()
          if vim.wo.diff then
            return ']c'
          end
          vim.schedule(function()
            gs.next_hunk()
          end)
          return '<Ignore>'
        end, { expr = true, buffer = bufnr, desc = 'Jump to next hunk' })
        map({ 'n', 'v' }, '<localleader>gp', function()
          if vim.wo.diff then
            return '[c'
          end
          vim.schedule(function()
            gs.prev_hunk()
          end)
          return '<Ignore>'
        end, { expr = true, buffer = bufnr, desc = 'Jump to previous hunk' })
      end,
    },
  },

  {
    -- Set lualine as statusline
    'nvim-lualine/lualine.nvim',
    -- See `:help lualine.txt`
    opts = {
      options = {
        icons_enabled = false,
        theme = 'auto',
        component_separators = '|',
        section_separators = '',
      },
    },
  },

  {
    -- Show bufferline
    'akinsho/bufferline.nvim',
    version = "*",
    dependencies = 'nvim-tree/nvim-web-devicons',
    opts = {
      options = {
        indicator = {
          icon = '▎', -- this should be omitted if indicator style is not 'icon'
          style = 'icon',
        }
      }
    }
  },

  {
    -- Add indentation guides even on blank lines
    'lukas-reineke/indent-blankline.nvim',
    -- Enable `lukas-reineke/indent-blankline.nvim`
    -- See `:help indent_blankline.txt`
    main = 'ibl',
    opts = {
      indent = { char = '▏' },
    },
  },

  -- Fuzzy Finder (files, lsp, etc)
  {
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = {
      'nvim-lua/plenary.nvim',
      -- Fuzzy Finder Algorithm which requires local dependencies to be built.
      -- Only load if `make` is available. Make sure you have the system
      -- requirements installed.
      {
        'nvim-telescope/telescope-fzf-native.nvim',
        -- NOTE: If you are having trouble with this installation,
        --       refer to the README for telescope-fzf-native for more instructions.
        build = 'make',
        cond = function()
          return vim.fn.executable 'make' == 1
        end,
      },
    },
    opts = {
      defaults = {
        -- path_display = 'truncate',
        layout_strategy = 'slim',
        layout_config = {
          width = 0.99,
          height = 101,
        },
        preview = {
          hide_on_startup = true,
        },
        file_ignore_patterns = { ".git/", "node_modules/" },
        vimgrep_arguments = {
          "rg",
          "--hidden",
          "--color=never",
          "--no-heading",
          "--with-filename",
          "--line-number",
          "--column",
          "--smart-case"
        }
      },
    }
  },

  {
    -- Highlight, edit, and navigate code
    'nvim-treesitter/nvim-treesitter',
    dependencies = {
      'nvim-treesitter/nvim-treesitter-textobjects',
      'nvim-treesitter/nvim-treesitter-refactor',
      'nvim-treesitter/nvim-treesitter-context',
    },
    build = ':TSUpdate',
  },

  {
    'epwalsh/obsidian.nvim',
    version = "*", -- recommended, use latest release instead of latest commit
    lazy = true,
    ft = "markdown",
    dependencies = {
      "nvim-lua/plenary.nvim",
    },
    opts = {
      disable_frontmatter = true,
      workspaces = {
        {
          name = "vault",
          path = "~/notes",
        },
      },
    },
  },

  {
    -- ZEN Mode
    "folke/zen-mode.nvim",
    opts = {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    }
  }

}, {})
-- }}}

-- OPTIONS {{{

-- set runtimepath
vim.opt.rtp:append(vim.fn.expand('~/.vim'))
vim.opt.rtp:append(vim.fn.expand('~/.vim/after'))

-- [[ Setting options ]]
-- See `:help vim.o`
-- NOTE: You can change these options as you wish!

-- Set highlight on search
vim.o.hlsearch = true

-- highlight cursor line
vim.o.cursorline = true

-- Make line numbers default
vim.wo.number = true

-- Enable mouse mode
vim.o.mouse = 'a'

-- Sync clipboard between OS and Neovim.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
vim.o.clipboard = 'unnamedplus'

-- Enable break indent
vim.o.breakindent = true

-- Save undo history
vim.o.undofile = true

-- Disable swap file
vim.opt.swapfile = false

-- allow hidden buffers to have unsaved work
vim.o.hidden = true

-- Case-insensitive searching UNLESS \C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Keep signcolumn on by default
vim.wo.signcolumn = 'yes'

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeoutlen = 300

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-- enable terminal colors
vim.o.termguicolors = true

-- nr of lines to keep on screen when scrolling
vim.o.scrolloff = 5

-- show character when breaking lines
vim.o.showbreak = '›'

-- create new buffers to the right or below
vim.o.splitbelow = true
vim.o.splitright = true

-- don't reopen file when already in tab
vim.o.switchbuf = 'usetab'

-- set tabstop to 2
vim.o.tabstop = 2

-- prevent emoji width issue. See https://www.youtube.com/watch?v=F91VWOelFNE&list=PLwJS-G75vM7kFO-yUkyNphxSIdbi_1NKX
vim.o.emoji = false

-- preserve EOL behaviour of file on save
vim.o.fixendofline = false

-- folding
vim.o.foldenable = false
vim.o.foldmethod = 'indent'
vim.o.foldlevel = 0

-- line breaks
vim.opt.linebreak = true
vim.opt.breakindent = true
vim.cmd('set breakindentopt=shift:2')

-- highlight whitespace characters
vim.o.list = false
vim.cmd('set listchars=nbsp:¬,tab:»·,trail:·,extends:>')

-- Filetype specific configuration
vim.cmd([[
  autocmd FileType md,markdown call init#markdown()
  autocmd FileType md,markdown call notes#init()
  autocmd BufEnter md,markdown call notes#init()
  autocmd FileType tex         call init#tex()
  autocmd FileType c,cpp,objc  call init#c()
  autocmd FileType mail        setlocal spell
]])

vim.cmd([[
  command! -nargs=0 CloseOtherBuffers :%bdelete!|edit #|.1,$bdelete
  command! -nargs=0 AutoSave autocmd TextChanged,TextChangedI <buffer> update
]])

-- remove trailing whitespace on save
vim.api.nvim_command([[ autocmd BufWritePre * %s/\s\+$//e ]])

-- restore last cursor position
vim.api.nvim_create_augroup("RestoreCursor", { clear = true })
vim.api.nvim_create_autocmd("BufReadPost", {
  group = "RestoreCursor",
  pattern = "*",
  callback = function()
    local line = vim.fn.line("'\"")
    if line >= 1 and line <= vim.fn.line("$")
      and vim.bo.filetype ~= "commit"
      and not vim.tbl_contains({ "xxd", "gitrebase" }, vim.bo.filetype) then
      vim.cmd("normal! g`\"")
    end
  end,
})

-- }}}

-- MAPPINGS {{{

-- [[ Basic Keymaps ]]

-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- Remap j/k for breaked lines
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- easier : commands
vim.keymap.set('n', ',', ':')

-- move to start/end of line
vim.keymap.set('n', 'gh', 'g^')
vim.keymap.set('n', 'gl', 'g$')

-- Quickly find a previous command
-- and avoid unintentional switches to Ex mode
vim.keymap.set('n', 'Q', 'q:?')

-- ctrl+backspace to delet word
vim.keymap.set('i', '<C-H>', '<C-W>')

-- [[ YANKING ]]

-- yank with Ctrl+c
vim.keymap.set('v', '<C-c>', '"+y')

-- yank to end of line
vim.keymap.set('n', 'Y', 'y$')

-- yank relative/absolute filepath
vim.keymap.set('n', 'y.', ':let @+ = expand("%")<CR>')
vim.keymap.set('n', '<leader>y', ':let @+ = expand("%:~:.")<CR>:echo "Yanked relative path"<CR>')
vim.keymap.set('n', '<leader>Y', ':let @+ = expand("%:p")<CR>:echo "Yanked absolute path"<CR>')

-- keep visual selection when indenting/outdenting
vim.keymap.set('v', '<', '<gv')
vim.keymap.set('v', '>', '>gv')

-- don't change register on visual paste
vim.cmd([[xnoremap p pgvy]])

-- [[ SEARCHING ]]

vim.keymap.set('n', 's', '/', { noremap = true })
-- search for selection
vim.keymap.set('v', 's', 'y/<C-R>"<CR>', { noremap = true })
-- search for selection, stay in search
vim.keymap.set('v', 'S', 'y/<C-R>"', { noremap = true })
-- search for yank, stay in search
vim.keymap.set('n', 'S', 'y/<C-R>"', { noremap = true })


-- [[ SPELL MAPPINGS ]]
vim.keymap.set('n', '<localleader>S', ':set spell!<CR>')
vim.keymap.set('n', '<localleader>sn', ']n')
vim.keymap.set('n', '<localleader>sp', '[n')
vim.keymap.set('n', '<localleader>sf', 'z=')
vim.keymap.set('n', '<localleader>sg', 'zg')
vim.keymap.set('n', '<localleader>sb', 'zb=')

-- suggest spelling correcting for mistake
vim.keymap.set('i', '<C-L>', '<Esc>[sea<C-x><C-s>', { noremap = true })

-- fix last spelling mistake for whole file in insert mode and jump back to last insert position
-- resetting the undo level ends the current undo block, allowing to rapid fire
-- this command while being able to undo the changes individually
vim.keymap.set('i', '<C-F>', '<Esc>[s 1z= :silent! spellr<CR> :let &undolevels=&undolevels<CR> gi', { noremap = true, silent = true })

-- jump to tag
vim.keymap.set('n', 'gt', '<C-]>zz')

-- [[ BUFFER ]]

-- Create a new file from the location of the buffer
vim.keymap.set(
  'n',
  '<localleader>n',
  [[:edit <C-R>=expand('%:p:h') . '/'<CR>]],
  { silent = true }
)

-- Remove the current file
vim.keymap.set(
  'n',
  '<localleader>bd',
  [[:call delete(expand('%')) <bar> bdelete!<CR>]],
  { silent = true }
)

-- Quit Vim after removing the current file
vim.keymap.set(
  'n',
  '<localleader>D',
  [[:call delete(expand('%'))<CR>]],
  { silent = true }
)


-- [[ NAVIGATION ]]

-- save/quit quickly
vim.keymap.set('n', '<leader>w', ':write!<CR>', { silent = true })
vim.keymap.set('n', '<leader>W', ':wall!<CR>', { silent = true })
vim.keymap.set('n', '<leader>q', ':quit<CR>', { silent = true })
vim.keymap.set('n', '<leader>Q', ':quit!<CR>', { silent = true })
vim.keymap.set('n', '<leader>d', ':cclose<CR> :bd!<CR>', { silent = true })
vim.keymap.set('n', '<leader>D', ':cclose<CR> :bd!<CR>', { silent = true })
vim.keymap.set('n', '<leader>o', ':only<CR>', { silent = true })
vim.keymap.set('n', '<leader>O', ':CloseOtherBuffers<CR>', { silent = true })
vim.keymap.set('n', '<leader>E', ':edit!<CR>', { silent = true })

-- force write with sudo
vim.keymap.set('c', 'w!!',
  [[execute 'silent! write !SUDO_ASKPASS=`which ssh-askpass` sudo tee % >/dev/null' <bar> edit!<CR>]],
  { noremap = true })

-- open Netrw file browser
vim.keymap.set('n', '-', ':Explore<CR>', { silent = true })

-- command mode moving
vim.keymap.set('c', '<C-a>', '<Home>', { noremap = true })
vim.keymap.set('c', '<C-e>', '<End>', { noremap = true })

-- quickfix navigation
vim.keymap.set('n', '<localleader>cn', ':cn<CR>')
vim.keymap.set('n', '<localleader>cp', ':cp<CR>')
vim.keymap.set('n', '<localleader>co', ':copen<CR>')

-- window navigation
vim.keymap.set('n', '<C-k>', ':wincmd k<CR>', { silent = true })
vim.keymap.set('n', '<C-j>', ':wincmd j<CR>', { silent = true })
vim.keymap.set('n', '<C-h>', ':wincmd h<CR>', { silent = true })
vim.keymap.set('n', '<C-l>', ':wincmd l<CR>', { silent = true })

-- buffer navigation
vim.keymap.set('n', '<A-k>', ':bn<CR>', { silent = true })
vim.keymap.set('n', '<A-j>', ':bp<CR>', { silent = true })

-- method navigation
vim.keymap.set('n', 'mn', ']m', { noremap = true })
vim.keymap.set('n', 'mp', '[m', { noremap = true })

-- window splits
vim.keymap.set('n', '<C-Right>', ':wincmd v<CR>', { silent = true })
vim.keymap.set('n', '<C-Down>', ':wincmd s<CR>', { silent = true })

-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

-- [[ MISC ]]

-- replace/substitute
vim.keymap.set('n', 'R', ':%s##gc<Left><Left><Left>', { noremap = true })
-- replace/substitute visual selection
vim.keymap.set('x', 'R', 'y:%s#<C-R>"##gc<Left><Left><Left>', { noremap = true })

-- dragon-drop current file
vim.keymap.set('n', '<localleader>dr',
  [[:w!<CR>:!GDK_BACKEND=x11 dragon-drop --and-exit %<CR>]],
  { noremap = true, silent = true })

-- easy search & replace (repeatable with dot)
vim.keymap.set('n', 'c*', '*Ncgn', { noremap = true, silent = true })

-- let %% expand to path of currently open file in command mode
vim.keymap.set('c', '%%', [[getcmdtype() == ':' ? expand('%:h').'/' : '%%']], { expr = true })

-- dot files management
vim.keymap.set('n', '<leader>ev', [[:edit `=resolve(expand("~/.config/nvim/init.lua"))`<CR>]], { silent = true })
vim.keymap.set('n', '<leader>eV', [[:edit `=resolve(expand("~/.vimrc"))`<CR>]], { silent = true })
vim.keymap.set('n', '<leader>ez', [[:edit `=resolve(expand("~/.config/zsh/.zshrc"))`<CR>]], { silent = true })
vim.keymap.set('n', '<leader>eI', [[:edit `=resolve(expand("~/.config/i3/config"))`<CR>]], { silent = true })
vim.keymap.set('n', '<leader>ei', [[:edit `=resolve(expand("~/.config/sway/config.d/custom-config.conf"))`<CR>]], { silent = true })
vim.keymap.set('n', '<leader>ex', [[:edit ~/.Xresources<CR>]], { silent = true })
vim.keymap.set('n', '<leader>sv', [[:w <bar> source ~/.config/nvim/init.lua<CR>]], { silent = true })
vim.keymap.set('n', '<leader>sV', [[:w <bar> source ~/.vimrc<CR>]], { silent = true })
vim.keymap.set('n', '<leader>sf', [[:w <bar> source %<CR>]], { silent = true })

-- move selected lines
vim.keymap.set('v', 'J', ":m '>+1<CR>gv=gv")
vim.keymap.set('v', 'K', ":m '<-2<CR>gv=gv")

-- enable ctrl+backspace to delete word in cmdmode
vim.keymap.set('c', '<C-H>', '<C-W>', { noremap = true })

-- [[ F-Keys ]]
vim.keymap.set('n', '<F1>', ':G | :1close<CR>', { silent = true })
vim.keymap.set('n', '<F3>', ':Copilot enable | Copilot! attach<CR>')
vim.keymap.set('n', '<F5>', ':w | terminal bash %<CR>', { silent = true })
vim.keymap.set('n', '<F6>', ':w | terminal pnpm run test:watch %<CR>', { silent = true })

-- [[ Preview files ]] {{{
vim.cmd([[
autocmd FileType *
            \ nmap <localleader>p :w <bar> ! xdg-open % & disown<CR><CR>
autocmd FileType latex,tex,md,markdown
            \ nmap <localleader>p :w <bar> ! $(xdg-open %<.pdf \|\| xdg-open %) &<CR><CR>
autocmd FileType html
            \ nmap <localleader>p :w <bar> ! firefox -new-window %<CR><CR>
]])

----- }}}

-- }}}

-- PLUGIN CONFIGURATION {{{

-- Vim-Fugitive {{{
vim.keymap.set('n', '<leader>gs', ':Git<CR>', { silent = true })
vim.keymap.set('n', '<leader>gr', ':Gread<CR>', { silent = true })
vim.keymap.set('n', '<leader>gw', ':Gwrite<CR>:Gstatus<CR>', { silent = true })
vim.keymap.set('n', '<leader>gc', ':Gwrite<CR>:Git commit -v<CR>', { silent = true })
vim.keymap.set('n', '<leader>gl', ':G log --oneline<CR>', { silent = true })
vim.keymap.set('n', '<leader>gll', ':Gclog<CR>', { silent = true })
vim.keymap.set('n', '<leader>ge', ':Gedit<CR>', { silent = true })
vim.keymap.set('n', '<leader>gd', ':Git difftool<CR>', { silent = true })
vim.keymap.set('n', '<leader>gD', ':Git difftool -y<CR>', { silent = true })
vim.keymap.set('n', '<leader>gm', ':Git mergetool<CR>', { silent = true })
vim.keymap.set('n', '<leader>gb', ':Git blame<CR>', { silent = true })
vim.keymap.set('n', '<leader>gF', ':Git fetch<CR>', { silent = true })
vim.keymap.set('n', '<leader>gL', ':Git pull<CR>', { silent = true })
vim.keymap.set('n', '<leader>gP', ':Git! push<CR>', { silent = true })
vim.keymap.set('n', '<leader>gB', ':GBrowse<CR>', { silent = true })
vim.keymap.set('n', '<leader>gg', ':Glcd<CR>', { silent = true })
vim.keymap.set('n', '<leader>gG', ':lcd %:h<CR>', { silent = true })


-- override mappings in fugitive windows
vim.cmd([[
  " commit without triggering husky hooks
  autocmd FileType fugitive nmap <buffer> cn :G commit -n<CR>
  autocmd FileType fugitive nmap <buffer> cn :G commit -n<CR>
  " populate branch delete command
  autocmd FileType fugitive nmap <buffer> gd :G branch -d<space>
  " populate rebase cmd
  autocmd FileType fugitive nmap <buffer> r<s-i> :G rebase -i HEAD~
  " commit without showing file modifications/insertions after commit window
  autocmd FileType fugitive nmap <buffer> cc :G commit --quiet<CR>
  autocmd FileType fugitive nmap <buffer> ca :G commit --quiet --amend<CR>
  autocmd FileType fugitive nmap <buffer> ce :G commit --quiet --amend --no-edit<CR>
  " navigate between chunks in status window
  autocmd FileType fugitive nmap <buffer> <localleader>gn ]c
  autocmd FileType fugitive nmap <buffer> <localleader>gp [c
  " navigate between chunks in diff mode
  autocmd User FugitiveBlob,FugitiveStageBlob nmap <buffer> <localleader>gn ]c
  autocmd User FugitiveBlob,FugitiveStageBlob nmap <buffer> <localleader>gp [c
  " disable mappings to avoid breaking diff screen
  autocmd User FugitiveBlob,FugitiveStageBlob nmap <buffer> <S-l> <nop>
  autocmd User FugitiveBlob,FugitiveStageBlob nmap <buffer> <S-h> <nop>
  " save commit msg
  autocmd FileType gitcommit nmap <silent> <buffer> <CR> :wq <bar> redraw!<cr>
]])

-- }}}

-- wilder.nvim {{{
local wilder = require('wilder')
wilder.set_option('pipeline', {
  wilder.branch(
    wilder.python_file_finder_pipeline({
      file_command = { 'rg', '--files', '--hidden' },
      filters = { 'fuzzy_filter', 'difflib_sorter' },
    }),
    wilder.cmdline_pipeline(),
    wilder.python_search_pipeline()
  ),
})

wilder.set_option('renderer', wilder.popupmenu_renderer({
  highlighter = wilder.basic_highlighter(),
}))

-- }}}

-- [[ Configure Telescope ]]
-- See `:help telescope` and `:help telescope.setup()`

vim.api.nvim_command('command! Files Telescope find_files hidden=true')

-- custom layout
require('telescope.pickers.layout_strategies').slim = function(picker, max_columns, max_lines, layout_config)
  local layout = require('telescope.pickers.layout_strategies').flex(picker, max_columns, max_lines, layout_config)
  layout.prompt.line = layout.prompt.line + 2
  layout.prompt.border = false
  layout.results.line = layout.results.line + 3
  layout.results.height = layout.results.height + 1
  layout.results.border = false
  if layout.preview then
    layout.preview.title = ''
    layout.preview.border = true
  end
  return layout
end

local select_one_or_multi = function(prompt_bufnr)
  local picker = require('telescope.actions.state').get_current_picker(prompt_bufnr)
  local multi = picker:get_multi_selection()
  if not vim.tbl_isempty(multi) then
    require('telescope.actions').close(prompt_bufnr)
    for _, j in pairs(multi) do
      if j.path ~= nil then
        vim.cmd(string.format("%s %s", "edit", j.path))
      end
    end
  else
    require('telescope.actions').select_default(prompt_bufnr)
  end
end

require('telescope').setup {
  defaults = {
    mappings = {
      i = {
        ["<ESC>"] = require('telescope.actions').close,
        ["<C-DOWN>"] = require('telescope.actions').cycle_history_next,
        ["<C-UP>"] = require('telescope.actions').cycle_history_prev,
        -- workaround to open multiple files. from https://github.com/nvim-telescope/telescope.nvim/issues/1048#issuecomment-1679797700
        ["<CR>"] = select_one_or_multi,
      },
    },
  },
}

-- Enable telescope fzf native, if installed
pcall(require('telescope').load_extension, 'fzf')

-- See `:help telescope.builtin`
vim.keymap.set('n', '<leader>?', require('telescope.builtin').oldfiles, { desc = '[?] Find recently opened files' })
vim.keymap.set('n', '<leader>b', require('telescope.builtin').buffers, { desc = '[ ] Find existing buffers' })
vim.keymap.set('n', '<leader>/', function()
  -- You can pass additional configuration to telescope to change theme, layout, etc.
  require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
    winblend = 10,
    previewer = false,
  })
end, { desc = '[/] Fuzzily search in current buffer' })

local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>f', function() builtin.find_files({ hidden = true }) end, { desc = '[S]earch [F]iles' })
vim.keymap.set('n', '<leader>F', function() builtin.find_files({ hidden = true, no_ignore = true }) end,
  { desc = '[S]earch All [F]iles' })
vim.keymap.set('n', '<leader>gf', builtin.git_files, { desc = 'Search [G]it [F]iles' })
vim.keymap.set('n', '<leader>h', builtin.help_tags, { desc = '[S]earch [H]elp' })
vim.keymap.set('n', '<leader>r', builtin.oldfiles, { desc = 'Search [O]old [F]iles' })
vim.keymap.set('n', '<leader>c', builtin.command_history, { desc = 'Search [C]command [H]istory' })
vim.keymap.set('n', '<leader>m', builtin.man_pages, { desc = '[S]earch [M]an Pages' })
vim.keymap.set('n', '<leader>sd', builtin.diagnostics, { desc = '[S]earch [D]iagnostics' })
vim.keymap.set('n', '<leader><leader>', builtin.resume, { desc = '[S]earch [R]esume' })

-- ripgrep
vim.keymap.set('n', '<leader>a', function()
  require('telescope.builtin').live_grep({
    disable_coordinates = true,
  })
end, { desc = '[S]earch current [W]ord' })

-- ripgrep word under cursor
vim.keymap.set('n', '<leader>A', function()
  require('telescope.builtin').live_grep({
    default_text = vim.fn.expand('<cword>'),
    disable_coordinates = true,
  })
end, { desc = '[S]earch by [G]rep' })

-- find and open markdown note
vim.api.nvim_command([[
  command! -nargs=0 -bang FindNotes call luaeval('require("telescope.builtin").find_notes()')
]])

require('telescope.builtin').find_notes = function()
  require('telescope.builtin').find_files({
    prompt_title = 'Notes',
    hidden = true,
    find_command = { 'rg', '--files', '--follow', '--type', 'md' },
  })
end

-- insert shell history command
require('telescope.builtin').shell_history = function()
  local histfile = os.getenv('HISTFILE') or os.getenv('HOME') .. '/.bash_history'
  local command = string.format("sed -e 's/: [0-9]*:0;//' -e '/^$/d' %s | tac", histfile)
  require('telescope.builtin').find_files {
    prompt_title = 'Shell History',
    find_command = { 'bash', '-c', command },
    attach_mappings = function(_, map)
      -- insert at current line
      map('i', '<CR>', function(prompt_bufnr)
        local selection = require('telescope.actions.state').get_selected_entry(prompt_bufnr)
        require('telescope.actions').close(prompt_bufnr)
        vim.fn.setline('.', { selection.value })
      end)
      return true
    end,
  }
end
vim.keymap.set('n', '<leader>R', require('telescope.builtin').shell_history, { desc = 'Search [S]earch [H]istory' })

-- [[ Configure Treesitter ]]
-- See `:help nvim-treesitter`
-- Defer Treesitter setup after first render to improve startup time of 'nvim {filename}'
vim.defer_fn(function()
  require('nvim-treesitter.configs').setup {
    -- Add languages to be installed here that you want installed for treesitter
    ensure_installed = { 'c', 'cpp', 'java', 'go', 'lua', 'python', 'rust', 'tsx', 'javascript', 'typescript', 'vimdoc',
      'vim' },

    -- Autoinstall languages that are not installed. Defaults to false (but you can change for yourself!)
    auto_install = false,

    highlight = { enable = true },
    indent = { enable = true },
    incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = '<c-space>',
        node_incremental = '<c-space>',
        scope_incremental = '<c-s>',
        node_decremental = '<M-space>',
      },
    },

    textobjects = {
      select = {
        enable = true,
        lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
        keymaps = {
          -- You can use the capture groups defined in textobjects.scm
          ['aa'] = '@parameter.outer',
          ['ia'] = '@parameter.inner',
          ['af'] = '@function.outer',
          ['if'] = '@function.inner',
          ['ac'] = '@class.outer',
          ['ic'] = '@class.inner',
        },
      },
      move = {
        enable = true,
        set_jumps = true, -- whether to set jumps in the jumplist
        goto_next_start = {
          [']m'] = '@function.outer',
          [']]'] = '@class.outer',
        },
        goto_next_end = {
          [']M'] = '@function.outer',
          [']['] = '@class.outer',
        },
        goto_previous_start = {
          ['[m'] = '@function.outer',
          ['[['] = '@class.outer',
        },
        goto_previous_end = {
          ['[M'] = '@function.outer',
          ['[]'] = '@class.outer',
        },
      },
      -- TODO: clashes with find al
      -- swap = {
      --   enable = true,
      -- swap_next = {
      -- ['<leader>a'] = '@parameter.inner',
      -- },
      -- swap_previous = {
      -- ['<leader>A'] = '@parameter.inner',
      -- },
      -- },
    },

    refactor = {
      highlight_current_scope = { enable = false },
    },
  }

  require 'treesitter-context'.setup {
    enable = true,
    max_lines = 0,            -- How many lines the window should span. Values <= 0 mean no limit.
    min_window_height = 0,    -- Minimum editor window height to enable context. Values <= 0 mean no limit.
    line_numbers = true,
    multiline_threshold = 20, -- Maximum number of lines to show for a single context
    trim_scope = 'outer',     -- Which context lines to discard if `max_lines` is exceeded. Choices: 'inner', 'outer'
    mode = 'cursor',          -- Line used to calculate context. Choices: 'cursor', 'topline'
    -- Separator between context and content. Should be a single character string, like '-'.
    -- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
    separator = nil,
    zindex = 20,     -- The Z-index of the context window
    on_attach = nil, -- (fun(buf: integer): boolean) return false to disable attaching
  }
end, 0)

-- Diagnostic keymaps
vim.keymap.set('n', '<localleader>ep', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' }) -- [d
vim.keymap.set('n', '<localleader>en', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })     -- ]d
-- vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })
-- vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = 'Open diagnostics list' })

-- [[ Configure LSP ]]
--  This function gets run when an LSP connects to a particular buffer.
local on_attach = function(_, bufnr)
  -- NOTE: Remember that lua is a real programming language, and as such it is possible
  -- to define small helper and utility functions so you don't have to repeat yourself
  -- many times.
  --
  -- In this case, we create a function that lets us more easily define mappings specific
  -- for LSP related items. It sets the mode, buffer and description for us each time.
  local nmap = function(keys, func, desc)
    if desc then
      desc = 'LSP: ' .. desc
    end

    vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
  end

  nmap('<localleader>rn', vim.lsp.buf.rename, '[R]e[n]ame')
  nmap('<localleader>f', vim.lsp.buf.code_action, '[C]ode [A]ction')

  -- quickfix
  local function quickfix()
    local selected = false
    vim.lsp.buf.code_action({
      apply = true,
      -- filter = function(a) return a.isPreferred end,
      filter = function(a)
        if not selected then
          selected = true
          return true  -- Only allow the first action
        end
        return false
      end
    })
  end
  vim.keymap.set('n', '<localleader>F', quickfix, { noremap=true, silent=true })

  nmap('gd', vim.lsp.buf.definition, '[G]oto [D]efinition')
  nmap('gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')
  nmap('gI', require('telescope.builtin').lsp_implementations, '[G]oto [I]mplementation')
  nmap('<leader>D', vim.lsp.buf.type_definition, 'Type [D]efinition')
  nmap('<leader>ds', require('telescope.builtin').lsp_document_symbols, '[D]ocument [S]ymbols')
  nmap('<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[W]orkspace [S]ymbols')

  -- See `:help K` for why this keymap
  nmap('K', vim.lsp.buf.hover, 'Hover Documentation')
  -- TODO: fix --
  -- nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')

  -- Lesser used LSP functionality
  nmap('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
  nmap('<leader>wa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
  nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
  nmap('<leader>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, '[W]orkspace [L]ist Folders')

  -- Create a command `:Format` local to the LSP buffer
  vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
    vim.lsp.buf.format()
  end, { desc = 'Format current buffer with LSP' })
end


-- document existing key chains
local wk = require("which-key")
wk.add({
  { "<leader>c", group = "[C]ode" },
  { "<leader>c_", hidden = true },
  { "<leader>d", group = "[D]ocument" },
  { "<leader>d_", hidden = true },
  { "<leader>g", group = "[G]it" },
  { "<leader>g_", hidden = true },
  { "<leader>h", group = "More git" },
  { "<leader>h_", hidden = true },
  { "<leader>r", group = "[R]ename" },
  { "<leader>r_", hidden = true },
  { "<leader>s", group = "[S]earch" },
  { "<leader>s_", hidden = true },
  { "<leader>w", group = "[W]orkspace" },
  { "<leader>w_", hidden = true },
})


-- [[ Configure LSP: Diagnostics ]]

-- disable intrusive virtual text
vim.diagnostic.config({ virtual_text = false })

-- LSP: Mason {{{

-- mason-lspconfig requires that these setup functions are called in this order
-- before setting up the servers.
require('mason').setup()
require('mason-lspconfig').setup()

-- Enable the following language servers
--  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
--
--  Add any additional override configuration in the following tables. They will be passed to
--  the `settings` field of the server config. You must look up that documentation yourself.
--
--  If you want to override the default filetypes that your language server will attach to you can
--  define the property 'filetypes' to the map in question.
local servers = {
  -- clangd = {},
  -- gopls = {},
  -- pyright = {},
  -- rust_analyzer = {},
  ts_ls = {},
  html = { filetypes = { 'html', 'twig', 'hbs' } },

  lua_ls = {
    Lua = {
      workspace = { checkThirdParty = false },
      telemetry = { enable = false },
    },
  },
}

-- Setup neovim lua configuration
require('neodev').setup()

-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Ensure the servers above are installed
local mason_lspconfig = require 'mason-lspconfig'

mason_lspconfig.setup {
  ensure_installed = vim.tbl_keys(servers),
}

mason_lspconfig.setup_handlers {
  function(server_name)
    require('lspconfig')[server_name].setup {
      capabilities = capabilities,
      on_attach = on_attach,
      settings = servers[server_name],
      filetypes = (servers[server_name] or {}).filetypes,
    }
  end,
}

-- }}}

-- [[ Configure nvim-cmp ]]
-- See `:help cmp`
local cmp = require 'cmp'
local luasnip = require 'luasnip'
require('luasnip.loaders.from_vscode').lazy_load()
luasnip.config.setup {}

cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert {
    ['<C-n>'] = cmp.mapping.select_next_item(),
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete {},
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_locally_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.locally_jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),

    ['<C-a>'] = cmp.mapping.complete({
      config = {
        sources = {
          { name = 'copilot' }
        }
      }
    }, { 'i', 's' })

  },
  sources = {
    { name = 'calc' },
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
    { name = 'buffer' },
    { name = 'path' },
    { name = 'copilot', group_index = 2 },
  },
}

-- [[ Configure vim-yoink ]]
vim.keymap.set('n', '<C-n>', '<Plug>(YoinkPostPasteSwapBack)')
vim.keymap.set('n', '<C-p>', '<Plug>(YoinkPostPasteSwapForward)')
vim.keymap.set('n', 'p', '<Plug>(YoinkPaste_p)')
vim.keymap.set('n', 'P', '<Plug>(YoinkPaste_P)')

-- [[ Configure zen-mode ]]
vim.keymap.set('n', '<leader>G', ':ZenMode<CR>', { silent = true })

-- [[ Configure Limelight ]]
vim.keymap.set('n', '<leader>L', ':Limelight!!<CR>', { silent = true })

-- [[ Theme ]]
vim.cmd('colorscheme base16-atelierforest')

-- [[ Cycle Colorschemes ]]
local colorschemes = vim.fn.getcompletion('', 'color')
local current_scheme = vim.api.nvim_cmd({ cmd = 'colorscheme' }, { output = true })
local colorschemes_idx = vim.fn.index(colorschemes, current_scheme)

local function change_colorscheme(forward)
    colorschemes_idx = (colorschemes_idx + (forward and 1 or -1) - 1) % #colorschemes + 1

    local ok, _ = pcall(vim.cmd, 'colorscheme ' .. colorschemes[colorschemes_idx])
    if not ok then
        change_colorscheme(forward)
    end

    print(colorschemes[colorschemes_idx])
end

vim.keymap.set('n', '<F2>', function() change_colorscheme(true) end)
vim.keymap.set('n', '<S-F2>', function() change_colorscheme(false) end)
