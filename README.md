# dot-files

My Linux configuration.

![screenshot](/uploads/94281c225d893cab97bf598dcdf8d481/image2.png)

## What I use

- Sway/Wayland, [Sway](sway/.config/sway/), [Waybar](waybar/.config/waybar/)
- Theming: [Wal](./shell/.config/wal-colors.sh), [theme.sh](./scripts/.local/bin/theme.sh)
- Firefox, [Tridactyl](browser/.config/tridactyl/tridactylrc)
- IDE: [Neovim, nvim-cmp, Fugitive, Telescope, Copilot](nvim/.config/nvim/init.lua), [Idea](JetBrains/)
- Terminals: [Foot](foot/.config/foot/foot.ini), [Alacritty](alacritty/.config/alacritty/alacritty.yml)
- Shell: [Bash](bash/.bashrc), [ZSH](zsh/.config/zsh/.zshrc)
- Aliases: [Aliases](shell/.config/aliasrc), [ZSH Aliases](zsh/.config/zsh/aliasrc)
- Fuzzy Finders: [FZF](shell/.config/fzfrc), [Rofi](rofi/)
- Terminal File Manager: [Ranger](ranger/.config/ranger/)
- Customization: [Profile](profile/.profile.custom), [Git](git/.gitconfig) [Keyboard Capslock remapping](keyboard/.keyboard)

Archive:
- [Neovim](nvim/.vimrc): COC, Fugitive, FZF
- X11: [I3-gaps](i3/.config/i3/config), [Polybar](polybar/.config/polybar/config.ini), scrot

---

## Setup
Use [GNU Stow](https://www.gnu.org/software/stow/) to symlink configuration files to corresponding directories.

Stow everything:
```sh
stow -t ~ $(ls -d */) -v
```

## Scripts

### Journaling
| Script | Description |
| ------ | ------ |
| [capture](./scripts/.local/bin/capture.sh) | capture thoughts to daily note with rofi
| [cal-dump.sh](./scripts/.local/bin/cal-dump.py) | Dump ICS calendar events into daily note
| [cal-stats.sh](./scripts/.local/bin/cal-stats.py) | Parse ICS calendar for total hours between given date

### Development tools
| Script | Description |
| ------ | ------ |
| [compile.sh](./scripts/.local/bin/compile.sh) | handle compiling different file-types |
| [daily-commits.sh](./scripts/.local/bin/daily-commits.sh) | print commits of the day |
| [suggest-commit-message.sh](./scripts/.local/bin/suggest-commit-message.sh) | suggest conventional commit messages |
| [clear-tex-build-files.sh](./scripts/.local/bin/clear-tex-build-files.sh) | remove all nonessential compiled files |
| [update-repos.sh](./scripts/.local/bin/update-repos.sh) | bulk-update git repositories |

### Workspace
| Script | Description |
| ------ | ------ |
| [workspace.sh](./scripts/.local/bin/workspace.sh) | Automate workspace setup |
| [sway-record.sh](./i3/.local/bin/sway-record.sh) | setup lecture recording on virtual montior |
| [open-fasd-file.sh](./scripts/.local/bin/open-fasd-file.sh) | Open recent files with rofi |
| [open.sh](./scripts/.local/bin/open.sh) | open files/directories/urls/stuff |
| [preview.sh](./scripts/.local/bin/preview.sh) | preview files in the terminal |
| [scrot-ocr.sh](./scripts/.local/bin/scrot-ocr.sh) | run ocr on screenshot |
| [scrot-qr.sh](./scripts/.local/bin/scrot-qr.sh) | parse qr code in screenshot |
| [scrot-translate.sh](scripts/.local/bin/scrot-translate.sh) | translate text |

### Utility
| Script | Description |
| ------ | ------ |
| [compress.sh](./scripts.local/bin/compress.sh) | handle compressing different file-types |
| [convert.sh](./scripts/.local/bin/convert.sh) | handle converting different file-types |
| [count-words.sh](./scripts/.local/bin/count-words.sh) | print most used words in text |
| [extract-audio.sh](./scripts/.local/bin/extract-audio.sh) | extract audio from files |
| [ffmpeg-join.sh](./scripts/.local/bin/ffmpeg-join.sh) | join videos using ffmpeg |
| [pdf-merge.sh](./scripts/.local/bin/pdf-merge.sh) | merge pdf files |
| [toggle-audio-output.sh](./scripts/.local/bin/toggle-audio-output.sh) | Switch audio source |
| [wacom.sh](./scripts/.local/bin/wacom.sh) | configure wacom tablet |
| [xournalpp-autocompile.sh](./xournalpp/.local/bin/xournalpp-autocompile.sh) | autocompile xournalpp to pdf |
| [i3-monitor.sh](./i3/.local/bin/i3-monitor.sh) | Monitor window events and run i3/sway commands upon matching event |
| [i3-toggle.sh](./i3/.local/bin/i3-toggle.sh) | Toggle floating windows in i3 |
