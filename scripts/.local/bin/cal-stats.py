#!/usr/bin/env python

# USAGE: Parse ICS calendar for total hours between date period
#
# EXAMPLES:
#
#   cal-stats.py [-q] [-s SKIP] DATE_FROM DATE_UNTIL [CALENDAR_FILE...]
#
#   cal-stats.py -q 2023-09-01 2023-09-30 ~/cloud/.Calendar-Backup/*10-01*.ics
#   cal-stats.py -q -s "Skip Event" 2023-09-11 2023-09-18 cal1.ics

import argparse
from ics import Calendar
from datetime import timedelta, datetime, timedelta
import pytz
from tabulate import tabulate
from pathlib import Path

## ARGUMENTS

# parse arguments
parser = argparse.ArgumentParser(description="Parse ICS calendar for total hours between date period")
parser.add_argument('-q', '--quiet', action='store_true', default=False, help='Enable quiet mode')
parser.add_argument('-s', '--skip', type=str, help='Skip events that contain this string')
parser.add_argument('-b', '--begins', type=str, help='Only events that begin with this string')
parser.add_argument('start_date', type=str, help='Start date in yyyy-mm-dd format')
parser.add_argument('end_date', type=str, help='End date in yyyy-mm-dd format')
parser.add_argument('calendar_files', nargs='+', type=str, help='List of calendar files')
args = parser.parse_args()

# Access the value of the argument and store it in a variable
quiet = args.quiet
skip_str = args.skip
begin_str = args.begins
date_from=datetime.strptime(args.start_date, "%Y-%m-%d")
date_to=datetime.strptime(args.end_date, "%Y-%m-%d")

## MAIN

nr_days = date_to - date_from
nr_hours = nr_days.days * 24

print(f'\nParsing events in {date_from} - {date_to} (days={nr_days.days}, hours={nr_hours})\n')

def load_calendar(file_name):
    # Load the .ics file
    with open(file_name, 'r') as f:
        calendar_data = f.read()

    if not quiet:
        print("CALENDAR", file_name, "\n")

    # Parse the calendar data
    return Calendar(calendar_data)


def parse_calendar(calendar):
    data=[]
    duration = timedelta()
    for event in calendar.events:
        if event.duration is None: continue

        if not(date_from.date() <= event.begin.date() <= date_to.date()): continue

        if skip_str is not None and event.description is not None:
            if event.description.find(skip_str, 0, -1) != -1:
                continue

        if begin_str is not None and event.name.startswith(begin_str) == False:
            continue

        data.append([event.duration, event.begin, event.end, event.name])
        duration += event.duration

        # cut off tasks going into next weeks
        if event.end.date() >= date_to.date():
            duration -= (event.end.date() - date_to.date())

    if not quiet:
        data = sorted(data, key=lambda x: x[1])
        print(tabulate(data, headers=["Duration", "From", "To", "Name"], tablefmt="simple"), "\n")

    return duration


summary_cals=[]
for cal_file in args.calendar_files:
    if not quiet: print('\n')
    calendar = load_calendar(cal_file)
    duration = parse_calendar(calendar)
    total_hours_in_hours = duration.total_seconds() / 3600
    total_hours_in_perc = total_hours_in_hours / nr_hours
    path = Path(cal_file).stem
    summary_cals.append([path, total_hours_in_hours, total_hours_in_perc])


summary_cals = sorted(summary_cals, key=lambda x: x[1], reverse=True)
print(tabulate(summary_cals, headers=["Calendar", "Total Hours", "In Percent"], tablefmt="simple"), "\n")
