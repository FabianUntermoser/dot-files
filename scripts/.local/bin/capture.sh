#!/bin/bash
 # shellcheck disable=SC2016,SC2034

# USAGE: capture to daily note with rofi

help() { while read -r; do printf '%s\n' "$REPLY"; done <<EOF
Usage: Capture user input into digital note
  ${0##*/} [--target TARGET] [--force] [--format FORMAT] [--top | --bottom | --regex REGEX]

TARGET:
  FILE      -- filepath to capture into
  DAILY     -- defaults file to "$DIARY_DIR/YYYY-MM-DD.md"
  WIP       -- capture into a file with tag #status/wip
  BOARD     -- capture into kanban board
  ACTIVE    -- capture into current active obsidian file
  OPEN      -- capture into current open obsidian files

FORMAT:
  LINE      -- format="\${TIME} \${INPUT}"
  LOG       -- format="- \${TIME} \${INPUT}"
  BOARD     -- format="- [ ] \${INPUT}"
  TASK      -- format="- [ ] \${TIME} \${INPUT}"
  DATE_LINE  -- format="[[\${DATE}]] \${TIME} \${INPUT}"
  DATE_TASK -- format="- [ ] [[\${DATE}]] \${TIME} \${INPUT}"

OPTIONS:
  --force   -- create file if it does not exit
  --top     -- insert at top of digital note
  --bottom  -- insert at bottom of digital note
  --regex   -- insert below regex match

REGEX
    #     -- first level heading
    ##    -- second level heading
    [[    -- wikilinks

EXAMPLES

  Default
  ${0##*/} --target DAILY --format TASK
  ${0##*/} --target ~/2AREAS/Journal/daily/${DATE}.md --format "- [ ] "

  Capture into Kanban Board beneath specific heading
  ${0##*/} --target BOARD --regex "^## "

  Add bullet point to current note
  ${0##*/} --target ACTIVE --format "- \${INPUT}"

  Log into open file beneath horizontal line
  ${0##*/} --target OPEN --format LOG --regex "---"

EOF
exit
}

err() {
    printf "${_RED}ERROR:${_RESET} %s\n\n" "$*" > /dev/stderr;
    notify-send "ERROR: $*"
}

die() { err "$*"; exit; }

escape() { printf "%q" "$@"; }

capture() {
    rofi -dmenu -i -matching glob -p "$*" -theme-str '@theme "onedark-capture"'
}

capture_select() {
    rofi -dmenu -i -matching glob -p "$*" -auto-select \
        -theme-str '@theme "onedark-capture"' \
        -theme-str 'listview { enabled: true; }'
}

## DEFAULTS

_RED=$(tput setaf 1)
NOTES_DIR="${NOTES_DIR:-$HOME/notes}"
DIARY_DIR="${DIARY_DIR:-$HOME/notes/2AREAS/Journal/daily}"
DELIM="|" # define sed delimiter to avoid user input clashes
DATE="$(date +%F)"
TIME="$(date +%H:%M)"
WS="WS $(i3-msg -t get_workspaces | jq -r '.[] | select(.focused==true).name'):"
FORMAT_LINE='${TIME} ${INPUT}'
FORMAT_LOG='- ${TIME} ${INPUT}'
FORMAT_BOARD='- [ ] ${INPUT}'
FORMAT_TASK='- [ ] ${TIME} ${INPUT}'
FORMAT_DATE_LINE='[[${DATE}]] ${TIME} ${INPUT}'
FORMAT_DATE_TASK='- [ ] [[${DATE}]] ${TIME} ${INPUT}'

## MAIN

[ -z "$1" ] && help

cd "$NOTES_DIR" || err "Could not enter $NOTES_DIR"

for arg; do
    case $arg in

        --top|--bottom) position="$1"; shift ;;

        --format) shift;
            case $1 in
                LOG)
                    prompt="Log"
                    format="$FORMAT_LOG";
                    ;;
                LINE)
                    prompt="Item"
                    format="$FORMAT_LINE";
                    ;;
                DATE_LINE)
                    prompt="Item"
                    format="$FORMAT_DATE_LINE";
                    ;;
                TASK)
                    prompt="New Task"
                    format="$FORMAT_TASK";
                    ;;
                BOARD)
                    prompt="New Task"
                    format="$FORMAT_BOARD";
                    ;;
                DATE_TASK)
                    prompt="New Task"
                    format="$FORMAT_DATE_TASK";
                    ;;
                *) format="$1"; ;;
            esac
            shift
            ;;

        --target) shift;
            target="$1";

            case $target in

                FILE) shift;
                    file=$(rg --files | rofi -dmenu -i -keep-right -matching glob -p "Select file")
                    ;;

                DAILY) shift;
                    file="$DIARY_DIR/$DATE.md";
                    position="${position:-"--bottom"}"
                    ;;

                WIP) shift;
                    file=$(rg "#status/wip" -l | rofi -dmenu -i -keep-right -matching glob -p "Select file")
                    ;;

                WORK) shift;
                    file=$(rg "#status/wip" -l | rofi -dmenu -i -keep-right -matching glob -p "Select file")
                    position="${position:-"beneath regex"}"
                    format="$FORMAT_TASK"
                    regex="$(escape "[[$DATE]]")"
                    ;;

                BOARD) shift;
                    format="$FORMAT_BOARD"
                    regex="##" ## match kanban board list headings
                    # capture to kanban board
                    board_postfix="Board.md"
                    file="$(find . -type f -regex ".*$board_postfix" \
                        -printf "\n%AT %p" | # print timestamp + file
                        sort -r | #sort most recent board first
                        awk '{ $1=""; print }' | # remove timestamp
                        sed 's/^ //g' | # trim leading whitespace
                        capture_select "To Board")"
                    ;;

                OPEN) shift;
                    # capture to currently open notes in obsidian
                    file="$(jq -r '.main.children[].children[].state.state.file' .obsidian/workspace.json | capture_select "Open Notes")"
                    ;;

                ACTIVE) shift;
                    # capture to currently active note in obsidian
                    file="$(jq -r '.active as $active | .main.children[].children[] | select(.id | contains($active) ) | .state.state.file' .obsidian/workspace.json)"
                    [ "$file" == "null" ] && file="$DIARY_DIR/$DATE.md";
                    position="${position:-"--bottom"}"
                    ;;

                *)
                    file="$target" ; shift
                    ;;

            esac
            ;;

        --force) shift
            # create file if not exists
            [ -z "$file" ] && die "file empty"
            [ -f "$file" ] || touch "$file"
            ;;

        --regex) shift;
            [ -f "$file" ] || die "section requires file"
            [ -z "$1" ] && die "no regex provided"
            position="beneath regex"
            regex="$(escape "$1")"
            grep -q -- "$regex" "$file" || continue

            # multiple matches found
            regex="$(grep -- "$regex" "$file" | capture_select "Match")"
            regex="$(escape "$regex")"
            shift;
            ;;

    esac
done

# By default, select note
[ -z "$file" ] && exit

prompt="$(basename "$file")"
prompt="${prompt//.md/}"

echo
echo "args: '$*'"
echo "file: '$file'"
echo "format: '$format'"
echo "position: '$position'"
echo "regex: '$regex'"

## QUICKFIX: sanitize clipboard for copying into rofi
command -v cliphist >/dev/null &&
    cliphist list | head -1 | cliphist decode | wl-copy

[ -f "$file" ] || die "File not found: $file"

if [ -n "$regex" ] && [ "$target" != "WORK" ]; then
    # abort on no match found
    grep -q -- "$regex" "$file" || die "regex '$regex' match not found"
fi

## CAPTURE

# get user input
INPUT="$(capture "$prompt")"
[ -z "$INPUT" ] && exit # exit on empty input

# set format based on user input
#   t -> Task
#   l -> Bullet Line
if [[ -z "$format" ]]; then
    if [[ "$INPUT" == "t "* ]]; then
        format="$FORMAT_TASK";
        INPUT="${INPUT:2}"
    elif [[ "$INPUT" == "T "* ]]; then
        format="$FORMAT_DATE_TASK";
        INPUT="${INPUT:2}"
    elif [[ "$INPUT" == "l "* ]]; then
        format="$FORMAT_LOG";
        INPUT="${INPUT:2}"
    elif [[ "$INPUT" == "L "* ]]; then
        format="$FORMAT_DATE_LINE";
        INPUT="${INPUT:2}"
    fi
    echo "format: '$format'"
fi

# default format
[ -z "$format" ] && format="$FORMAT_LINE";

# evaluate variables in format string
text="$(eval "echo ${format}")"

## INSERT
if [ -n "$regex" ]; then

    if [ "$target" = "WORK" ]; then
        grep -q -- "$regex" "$file" || {
            # insert date heading after regex pattern
            sed -i "s${DELIM}---${DELIM}&\n${regex}\n${DELIM}g" "$file"
        }
    fi

    grep -q -- "$regex" "$file" || die "regex '$regex' not matched"

    # append after regex pattern
    sed -i "s${DELIM}${regex}${DELIM}&\n${text}${DELIM}g" "$file"

else

    if [ "$position" != "--bottom" ]; then
        # insert to top of file
        { echo "${text}"; cat "$file"; } | tee "$file" > /dev/null
    else
        # append to end of file
        echo "" >> "$file"
        eval echo \""${text}"\" >> "$file"
    fi
fi
