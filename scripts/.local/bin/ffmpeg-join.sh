#!/bin/sh

# USAGE: join videos using ffmpeg

tmp_file=fileList.txt
cleanup() { rm "$tmp_file"; exit 0; }
trap cleanup 0 1 2 3 6

echo "# position the files in the order they should be joined" > "$tmp_file"
for f; do
    echo "file '$f'" >> "$tmp_file"
done

# allow user to edit ordering of video files
vim "$tmp_file"

# join video
file_name=$(date -u +"%Y-%m-%d")-output.mp4
echo "Joining video files in $file_name"
ffmpeg -f concat -safe 0 -i "$tmp_file" -c copy "$file_name" -loglevel error
