#!/usr/bin/env bash

# converts based on file extension

set -e

help() {
    while read -r; do printf '%s\n' "$REPLY"; done <<-EOF
	Usage:
	  ${0##*/} [FILE...]

	Convert Files based on Extension.

	Supported Extensions:
	* .ppt|.ppsx        -> convert to pdf
	* .odt|.doc|.docx   -> convert to pdf
	* .xopp             -> convert to pdf
	* .m4a              -> convert to mp3
	EOF
    exit
}

err() { echo "$@" > /dev/stderr; exit; }

convert() {
    file=$(readlink -f "$1"); shift;
    dir=$(dirname "$file")
    cd "$dir" || exit

    echo "converting ${file##*/} ..."

    case "$file" in
        *\.ppt* | *\.ppsx | \
        *\.odt | *\.doc*) unoconv -f pdf "$file" ;;
        *\.m4a) ffmpeg -v 5 -y -i "$file" -acodec libmp3lame "${file/%m4a/mp3}" ;;
        *\.xopp) xournalpp -p "${file/%xopp/pdf}" "$file" ;;
        *) echo "format not supported"
    esac
}

[ $# = 0 ] && err "input files required"

for arg in "$@"; do
    convert "$arg"
done
