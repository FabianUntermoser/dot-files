#!/bin/sh

# USAGE: Open file indexed by fasd with a default program

fasd -f -R -l | # get most recent files
    rofi -dmenu -i -keep-right -matching glob -p "Open a file" |  # select file
    xargs -I % \
    setsid -f xdg-open "%" # open file with default application
