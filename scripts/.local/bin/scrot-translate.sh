#!/usr/bin/env bash

# select rectangle on screen and copy ocr text to clipboard
scrot --select --overwrite --exec "tesseract \$f - | xsel -b && rm \$f"

# get clipboard
txt=$(xsel -b)

notify-send "$(trans -b "$txt")"
