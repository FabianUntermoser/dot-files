#!/bin/sh

# USAGE: various fixes that I have to run after hibernation/cold start issues
# - Fix fab-pc weird monitor issue after coming back from sleep by changing monitor refresh rate
# - Fix fab-pc audio not working after hibernating
# - Increase fab-laptop resolution

log() { printf "%s" "$*"; notify-send "$*"; }

set_modeline() {
    modeline_args=$(gtf "$@") # replace with other resolution if necessary
    modeline_name=$(echo "$modeline_args" | grep Modeline | awk '{ print $2 }' | sed -e 's/"//g' -e 's/.00//')
    modeline_cmd=$(echo "$modeline_args" | grep Modeline | awk '{ $1=""; print }')
    primary_output=$(xrandr --current | grep " connected" | awk '{print $1}')

    xrandr --current | grep -q "$modeline_name" || {
        eval xrandr --newmode "$modeline_cmd"
        eval xrandr --addmode "$primary_output" "$modeline_name"
    }

    xrandr --current | grep "$modeline_name" | grep -q '*' && {
        log "modeline %sx%s %shz already set\n" "$@"
        return 1
    } || {
        log "setting modeline %sx%s %shz\n" "$@"
        xrandr --output "$primary_output" --mode "$modeline_name"
    }
}

[ "$HOSTNAME" == "fab-pc" ] && {
    echo "hostname: pc"

    # prevent being stuck in caps loc
    setxkbmap -option
    source "$HOME/.keyboard"

    # set_modeline 2560 1440 70 || set_modeline 2560 1440 80


    # Modeline "2560x1440_70.00" 369.35 2560 2760 3040 3520 1440 1441 1444 1499 -HSync +Vsync
    xrandr --output DisplayPort-1 --mode 2560x1440
    xrandr --delmode DisplayPort-1 2560x1440_70
    xrandr --newmode 2560x1440_70  369.35 2560 2760 3040 3520 1440 1441 1444 1499 -HSync +Vsync
    xrandr --addmode DisplayPort-1 2560x1440_70
    xrandr --output DisplayPort-1 --mode 2560x1440_70

}

[ "$HOSTNAME" == "fab-laptop" ] && {
    echo "hostname: laptop"

    # set_modeline 2560 1440 60

    # Modeline "2560x1440_60.00"  311.83  2560 2744 3024 3488  1440 1441 1444 1490  -HSync +Vsync
    xrandr --newmode 2560x1440_60  311.83  2560 2744 3024 3488  1440 1441 1444 1490  -HSync +Vsync
    xrandr --addmode eDP-1 2560x1440_60
    xrandr --output eDP-1 --mode 2560x1440_60
    xrandr --delmode eDP-1 2560x1440_60

}
