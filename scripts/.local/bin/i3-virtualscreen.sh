#!/usr/bin/env bash

# Create a "virtual" monitor with xrandr.
# This is useful for simulating a multi-monitor setup.
# I use this to record lectures on the virtual monitor with OBS.
# This allows me to work on my physical monitor, without interrupting the recording.

# i3-virtualscreen.sh                  -> Prints some output.
# i3-virtualscreen.sh --off            -> "Unplug" the virtual monitor.
# i3-virtualscreen.sh --on [up | left] -> Sets up the virtual monitor.
# i3-virtualscreen.sh --on [right PADDING | down PADDING ] -> Adds padding

primary_output=$(xrandr --current | grep " connected" | awk '{print $1}')
echo "primary output: $primary_output"

virtual_output=$(xrandr --current | grep "disconnected" | awk 'NR==1{print $1}')
echo "virtual output: $virtual_output"

current_mode=$(xrandr --current |
    grep '\*' | # get current mode
    awk '{print $1, $2}' | # get resolution & refresh rate
    head -n 1) # limit to first active modeline (primary is listed first)
resolution=$(echo "$current_mode" |
    awk '{print $1}' | # get resolution
    sed 's/x/ /' | # seperate x & y
    sed 's/\(.*\)_.*/\1/') # sanitize resolution
refresh_rate=$(echo "$current_mode" | awk '{ print $2 }' | sed 's/\..*//')
echo "current mode: $resolution ${refresh_rate}hz"

echo "getting modeline with: gtf $resolution $refresh_rate"
modeline=$(eval gtf "$resolution" "$refresh_rate" | grep Modeline | awk '{$1=""; print}')
mode=$(echo "$modeline" | awk '{print $1}')
echo "mode: $mode"

[ "$1" = "--off" ] && {
    eval xrandr --output "$virtual_output" --off
    eval xrandr --delmode "$virtual_output" "$mode" 2>/dev/null
    # eval xrandr --rmmode "$mode" 2>/dev/null
    exit
}

[ "$1" = "--on" ] || exit
args="${2:-"--right-of $primary_output"}"
padding="${3:-0}"

# get primary x & y pos
eval set -- "$resolution"

# position virtual output
case $args in
    right) args="--pos $(($1+padding))x0" ;;
    down) args="--pos 0x$(($2+padding))" ;;
    # some applications open on the left/top most output
    up) args="--above $primary_output" ;;
    left) args="--left-of $primary_output" ;;
esac

[ -z "$virtual_output" ] && {
    echo "$virtual_output not valid"
    exit
}

# setup virtual monitor
eval xrandr --newmode "$modeline"
eval xrandr --addmode "$virtual_output" "$mode"
eval xrandr --output "$virtual_output" --mode "$mode" "$args"

# make sure primary output is correct
xrandr --output "$primary_output" --primary

notify-send "Using $virtual_output as a virtual monitor"
