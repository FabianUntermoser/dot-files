#!/usr/bin/env bash

CONFIG_DIR=$XDG_CONFIG_HOME/workspaces
[ ! -d "$CONFIG_DIR" ] && CONFIG_DIR="$HOME"/.config/workspaces
LAYOUT_DIR="$CONFIG_DIR"/layouts
PROFILE_DIR="$CONFIG_DIR"/profiles
declare -A PROFILES=()
VERBOSE=false
SELECT=false
#  check if runnin inside terminal
[ -t 1 ] && TTY=1

msg() { notify-send "$*"; echo -e "\033[0;31m$1\033[0m ${*:2}" >&2; }
die() { notify-send "$*"; echo -e "\033[0;31m$1\033[0m ${*:2}" >&2; exit 1; }
i3-msg(){
    command i3-msg "$@" 2>/dev/null;
    [ $? -eq 127 ] && swaymsg "$@" >/dev/null;
}
log() { test "$VERBOSE" = true && echo "$@" >&2; }
cmd_exists() { command -v "$1" > /dev/null; }
is_x11() { [ "$XDG_SESSION_TYPE" == "x11" ]; }

help() {
    while read -r; do printf '%s\n' "$REPLY"; done <<-EOF
	Usage:
	  ${0##*/} [-h] [-v] [-s | -p PROFILE ARGS... ]

	  Setup your workspace in i3/Sway.
	  Files are stored in '\$XDG_CONFIG_HOME/workspaces', or '\$HOME/.config/workspaces'.

	Options:
	  -h | --help       -> print this help
	  -v | --verbose    -> enable logging
	  -p | --profile    -> launch WORKSPACE with ARGS
	  -s | --select     -> let user select WORKSPACE

	Functions:
	  The following helper functions are available in PROFILE scripts:

	  * run [-o PID] [-t INSTANCE_NAME] CMD
	    -> execute CMD with i3
	         -o | --once     -> run CMD with i3/Sway if theres no process running with optional PID or CMD
	         -t | --terminal -> run CMD in new terminal with given instance name.

	  * menu -> provides user selection with dmenu like programs.
	            example: 'menu "prompt text" item1 item2 ...'

	  * menu_provider -> override menu program
	                     example: 'menu_provider() { echo "dmenu -p"; }'
	                              'menu_provider() { echo "fzf --prompt"; }'
	                              'menu_provider() { echo "rofi -dmenu -p"; }'

	  * menu_validate -> override menu validation
	                     example:
	                        menu_validate() {
	                            [ -z "\$1" ] && return 1
	                                return 0;
	                            }

	  * switch_workspace [NAME]
	    -> switch to the next free workspace.
	       when providing a name, the format '1: [NAME]' will be used.


	  * append_layout [NAME]
	    -> apply custom workspace layout if it exists
           by default infers layout file name from profile name

	  * wait_for_network
	    -> wait for internet connectivity (default: 3 retries)
	EOF
    exit 0;
}

run() {
    case $1 in
        -o | --once) shift;
            local cmd id="$1"
            [ -n "$2" ] && cmd="$2" || cmd="$1"
            [ -z "$(pidof "$id")" ] && run "$cmd";
            return 0;
            ;;
        -t | --terminal) shift;

            case $TERMINAL in
                alacritty)
                    i3-msg "exec --no-startup-id \"alacritty --class '$1' -e $SHELL -c '$2'\"";
                    ;;
                *)
                    i3-msg "exec --no-startup-id \"i3-sensible-terminal -n '$1' -e $SHELL -c '$2'\"";
                    ;;
            esac

            ;;
        -*) help ;;
        *) i3 exec "$@"; ;;
    esac
}

init_profiles() {
    for file in "$PROFILE_DIR"/* ; do
        name=${file##**/}
        name=${name%.sh}
        log "found profile $name with layout file $file"
        PROFILES+=([$name]=$file)
    done
}

menu() {
    local menu prompt_text
    prompt_text="$1" && shift;
    items=${*:-$(cat /dev/stdin)}
    menu="$(menu_provider)"
    selection='-'

    if [ -n "$menu" ]; then
        while [ -n "$selection" ] && ! menu_validate "$selection"; do
            selection=$(printf '%s\n' "$items" | $menu "$prompt_text")
        done
    else
        PS3="$prompt_text"
        select selection in "$@"; do
            menu_validate "$selection" || continue
            log "selection: $selection"
            break
        done
    fi

    echo "$selection"
}

menu_provider() {
    [ -z "$WAYLAND_DISPLAY" ] && [ $TTY ] && cmd_exists fzf && echo "fzf -i --no-sort --prompt" && return
    cmd_exists rofi && echo "rofi -matching glob -dmenu -i -p" && return
    cmd_exists dmenu && echo "dmenu -i -l 20 -p" && return
}

menu_validate() {
    [ -z "$1" ] && return 1;
    [ ! -f "${PROFILES[$1]}" ] && return 2;
    return 0;
}

select_profile() {
    echo "selecting profiles"
    PROFILE=$(printf '%s\n' "${!PROFILES[@]}" | sort | menu "Select a profile ")
    menu_validate "$PROFILE" || exit # when menu was escaped
}

is_workspace_used () {
    [ -z "$1" ] && return 1;
    log "validating workspace: $1"

    if is_x11; then
	# check if workspace is empty
	window_count=$(i3-msg -t get_tree |
	    jq '.nodes[].nodes[].nodes[] | select(.type=="workspace")' | # get workspace nodes
	    jq "select(.num==$1).nodes | length"  # get worspace with positive numbers (negative is scratchpad)
	)
    else
	# check if workspace is empty
	window_count=$(swaymsg -t get_tree |
	    jq '.nodes[].nodes[] | select(.type=="workspace")' | # get workspace nodes
	    jq "select(.num==$1).nodes | length"  # get worspace with positive numbers (negative is scratchpad)
	)
    fi

    log "workspace has $window_count nodes"
    return "$window_count"
}

current_workspace() {
    i3-msg -t get_workspaces |
        jq '.[] | select(.focused==true).num'
}

find_gap_in_workspace() {
    i3-msg -t get_workspaces |
        jq '.[] | .num' |
        awk '$1!=p+1{print p+1;exit}{p=$1}'
}

next_free_workspace() {
    ws=$(find_gap_in_workspace)
    [ -z "$ws" ] && i3-msg -t get_workspaces | jq '.[-1] | .num + 1'
    echo "$ws"
}

switch_workspace() {
    local name;
    [ -n "$1" ] && name=": $1"
    log "switching to workspace '${WORKSPACE}${name}'"
    i3 workspace "${WORKSPACE}${name}";
}

append_layout() {
    LAYOUT_FILE="$LAYOUT_DIR/${1:-$PROFILE}".json
    # [ -f "$LAYOUT_FILE" ] || die "no layout file found:" "$LAYOUT_FILE"
    log "found layout file: $LAYOUT_FILE"
    i3 append_layout "$LAYOUT_FILE"
}

wait_for_network(){
    attempts="${1:-3}"
    until [[ $(ping -q -c 1 -W 1 1.1.1.1) ]]; do
        cmd_exists notify-send &&
            notify-send "waiting for network connection ..."
        echo "waiting for network connection ..."
        [ "$retries" == "$attempts" ] && return 1
        sleep 3
        retries=$((retries+1))
    done
    return 0
}

parse_arguments(){
    [ -z "$1" ] && help

    for arg in "$@"; do
        case $1 in
            -v | --verbose) shift; VERBOSE=true; ;;
            -p | --profile) PROFILE="$2"; shift 2; ;;
            -s | --select) shift; SELECT=true;
                [ "$#" != 0 ] && help; ;;
            -h | --help | -*) help ;;
        esac
    done;
}

parse_arguments "$@"
init_profiles

test "$SELECT" = true && select_profile
menu_validate "$PROFILE" || die "invalid profile:" "'$PROFILE'"

WORKSPACE=$(current_workspace)
is_workspace_used "$WORKSPACE" || WORKSPACE=$(next_free_workspace)

# source profile script
log "executing profile script for '$PROFILE': ${PROFILES[$PROFILE]}"

# shellcheck source=/dev/null
. "${PROFILES[$PROFILE]}"
