#!/bin/sh

# color codes
_RED=$(tput setaf 1)
_BLUE=$(tput setaf 4)
_GREEN=$(tput setaf 2)
_GOLD=$(tput setaf 3)
_BOLD=$(tput bold)
_RESET=$(tput sgr0)

set -e

help() {
    while read -r; do printf '%s\n' "$REPLY"; done <<-EOF
	Usage:
	  ${0##*/} [FILE...]

	Compresses Files based on Extension.
	EOF
    exit
}

err() { printf "${_RED}ERROR:${_RESET} %s\n\n" "$*" > /dev/stderr; }
die() { err "$*"; help; exit; }
size_of() { du -h "$1" | cut -f 1; }

convert() {
    file=$(readlink -f "$1"); shift;
    dir=$(dirname "$file")
    base=${file%.*}
    ext=${file##*.}
    name=${file##*/}
    newname=${base}-compressed.${ext}

    [ ! -f "$file" ] && err "not a file" && return
    cd "$dir" || return

    printf "compressing ${_BOLD}%s${_RESET}\n" "$name"

    shopt -s nocasematch
    case "$file" in
        *\.mp4|*\.mkv|*\.mov) ffmpeg -y -i "$file" -vcodec libx264 -crf 28 "$newname" ;;
        *\.jpg) ffmpeg -y -i "$file" "$newname" -loglevel quiet ;;
        *\.pdf)
            # converting to ps and then to pdf reduces size
            pdf2ps "$file" "$base".ps
            ps2pdf "$base".ps "$newname"
            rm "$base".ps
            ;;
        *) err "format not supported" && return;
    esac

    # show file size after compressing
    printf "compressed ${_BOLD}%s${_RESET} (%s -> %s)\n" "$name" "$(size_of "$file")" "$(size_of "$newname")"
}

[ $# = 0 ] && die "No Files provided"

for arg in "$@"; do
    convert "$arg"
done
