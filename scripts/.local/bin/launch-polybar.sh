#!/bin/bash

# return if polybar not installed
command -v polybar || exit

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location ~/.config/polybar/config.ini
setsid -f polybar -r default &

echo "Polybar launched..."
