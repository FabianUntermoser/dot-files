#!/usr/bin/env sh

# USAGE: Launch syncthing while avoiding sync conflicts on my digital notes
# 1. Check git status of note repos
# 2. If dirty, create a temporary commit
# 3. Git pull
# 4. If rebase/merge, allow user to resolve conflicts
# 5. Start syncthing

log() {
    printf "%s" "$*";
    notify-send "$*";
}

die() {
    log "$@" 1>/dev/stderr;
    exit 1;
}

is_rebasing() {
    [ -d .git/rebase-merge ]
}

commit_msg="${0##*/}: temporary commit"

# exit if syncthing is already running
pgrep -x syncthingtray >/dev/null && die "Syncthing already running"

cd ~/notes || die "can't enter ~/notes"

# detect if repo is in invalid state
is_rebasing && die "Rebase/Merge in progress ..."

# create temporary commit if needed
if [ "$(git status -s)" ]; then
    # create temporary commit for updating
    log "Repo is dirty. Creating temporary commit ..."
    git add . || die "git error: can't add files"
    git commit -m "$commit_msg" || die "git error: can't commit files"
fi

# update notes
if ! git pull; then
    log "Git pull failed. Check for conflicts or network issues."

    # detect conflicts
    is_rebasing && log "Conflicts detected"

    # open file with conflicts in nvim difftool
    git status -s | awk '/UU/ { $1=""; print } ' |
        xargs -I % "$TERMINAL" -e nvim +Gvdiffsplit! % \;
fi

if [ "$(git status -s)" ]; then
    log "Repo still dirty"
    exit 1
fi

# soft reset to previous state
if git log --oneline -1 | grep -q "$commit_msg"; then
    git reset HEAD~1
fi

log "Starting Syncthingtray ..."
setsid -f syncthingtray --wait
