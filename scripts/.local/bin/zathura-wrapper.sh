#!/bin/sh

# USAGE: launch PDFs directly from obsidian without blocking it
#
# find out default application for pdf
#   xdg-mime query default application/pdf
#
# update the .desktop (i.e org.pwmt.zathura-pdf-mupdf.desktop) file to call this script

zathura "$1" & disown
