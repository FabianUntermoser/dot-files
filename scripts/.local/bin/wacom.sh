#!/bin/sh

# Auto run this script on Wacom device detection (USB & Bluetooth).
# Requires udev rule with wrapper script:
# ```
# echo 'ACTION=="add", SUBSYSTEM=="hid", DRIVERS=="wacom", RUN+="/usr/bin/setup-wacom.sh"' | sudo tee /etc/udev/rules.d/99-wacom.rules
# echo '#!/bin/sh' | sudo tee /usr/bin/setup-wacom.sh
# echo "$(where wacom.sh) &" | sudo tee -a /usr/bin/setup-wacom.sh
# sudo chmod a+x /etc/udev/rules.d/99-wacom.rules
# sudo chmod a+x /usr/bin/setup-wacom.sh
# sudo udevadm control --reload-rules
# ```

# debugging
# log="/tmp/udev-wacom-log-$(date +"%H-%M-%S").txt"
# exec > "$log" 2>&1

# detect X11 env (when run from udev rule)
export DISPLAY=:0
export XAUTHORITY=/home/fab/.Xauthority

# wait for wacom registering
for i in 1 2 3; do
    [ -z "$(xsetwacom list)" ] || break;
    [ "$i" = 3 ] && exit
    sleep 1;
done

# wacom devices
pad=$(xsetwacom list devices | grep PAD | sed 's/.*id: \([0-9]*\).*/\1/g')
stylus=$(xsetwacom list devices | grep STYLUS | sed 's/.*id: \([0-9]*\).*/\1/g')

# fallbacks
[ -z "$pad" ] && pad="Wacom Intuos BT S Pad pad"
[ -z "$stylus" ] && pad="Wacom Intuos BT S Pen stylus"

/bin/echo "mapping Stylus to primary Output eDP-1"
xsetwacom set "$stylus" MapToOutput eDP-1 ||
    xsetwacom set "$stylus" MapToOutput HEAD-0

# custom keybinding to toggle PEN and HAND tool
/bin/echo "mapping Button 1 to Ctrl+s"
xsetwacom set "$pad" Button 1 "key +ctrl +shift b -ctrl -shift"

# switch to latest i3 workspace
/bin/echo "mapping Button 2 to MOD+b"
xsetwacom set "$pad" Button 2 "key +super"

# undo
/bin/echo "mapping Button 3 to Ctrl+z"
xsetwacom set "$pad" Button 3 "key +ctrl z -ctrl"

# redo
/bin/echo "mapping Button 4 (8) to Ctrl+y"
xsetwacom set "$pad" Button 8 "key +ctrl y -ctrl"
