#!/bin/sh

dir_git_hook=".git/hooks/prepare-commit-msg"

usage() {
    while read -r; do printf '%s\n' "$REPLY"; done <<-EOF
	Usage:
	  ${0##*/} [-h] [-i] REPO

	Prints suggested commit messages for the given REPO based on its commit history.

	Suggestions are made using the conventional commit message style such as: 'type(scope): '.
	The commit history for staged files is searched for the most used types and scopes.
	The top results are printed for quicker vim committing with the fugitive plugin.

	Example Suggestions:
	  docs(README):
	  feat(scripts):
	  # - suggestions from prepare-commit-message.sh

	Options:
	  -h | --help       -> print this help
	  -i | --install    -> install git hook for REPO

	EOF
    exit 0;
}

die() { echo "error: $*"; exit; }

_install_git_hook() {
    [ -d "$1/.git" ] || die "'$1' is not a repository"

    echo "installing git hook to '$repo/$dir_git_hook'"

    hook="$1/$dir_git_hook"
    cat <<-EOF > "$hook"
	#!/bin/sh

	# USAGE: git hook to prepend suggested commit messages from ${0##*/}

	COMMIT_MSG_FILE=\$1

	# only suggest commits when not in rebase
	if [ "\$(sed -e '/^#.*/d' -e '/^$/d' "\$COMMIT_MSG_FILE" | wc -l)" = 0 ]; then
	    txt=\$(cat "\$COMMIT_MSG_FILE")
	    # append suggested commits
	    ${0##*/} . > "\$COMMIT_MSG_FILE"
	    # append previous commit message template
	    echo "\$txt" >> "\$COMMIT_MSG_FILE"
	fi
	EOF

    # ensure hook is executable
    chmod +x "$hook"
}

_top_result() { sort | uniq -c | sort -r | awk '{ print $2 }';}

_suggest_commit_msg() {
    # get commits that touched given filename
    commits=$(
        git log --follow --format=format:%s -- "$staged_file" | # commits that touched the file
            grep '(' | grep ':' # filter out non-conventional commits
    )
    # abort if no commits found
    [ -z "$commits" ] && exit
    # get most used type from commits
    types=$(echo "$commits" | cut -d':' -f1 | sed -e 's#\(.*\)(.*#\1#g' | _top_result)
    # get most used scope from commits
    scopes=$(echo "$commits" | cut -d':' -f1 | sed  -e's#.*(\(.*\)).*#\1#g' | _top_result)
    # prepare commit message
    t=$(echo "$types" | head -1)
    s=$(echo "$scopes" | head -1)
    echo "${t}(${s}): "
}

# parse arguments
[ -z "$1" ] && usage
for opt; do
    case $opt in
        -i|--install)
            repo=$2
            _install_git_hook "$repo"
            shift 2;
            exit;
            ;;
        -h|-*) shift; usage; ;;
    esac
done

repo=$1
[ -d "$repo/.git" ] || die "'$repo' is not a git repository"

cd "$repo" || die "can't cd to '$repo'"

# provide conventional commit suggestion for each staged file
git diff --name-only --cached |
    while read -r staged_file; do
        _suggest_commit_msg "$staged_file"
    done | uniq

# display last few commit messages
printf '\n# Commit Suggestions\n'
git log -n 5 --pretty=format:"# %s"
# show usage
printf "\n#\n# - %s\n\n" "${0##*/}"
