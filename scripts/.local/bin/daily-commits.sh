#!/bin/sh

# USAGE: list previous commits SINCE from repos in DIR
# - DIR ~/repos
# - SINCE (1 hour ago, 2 day ago, a week ago, ...)

die() { echo "error: $*" >&2; exit 1; }

since="$*"
[ -z "$1" ] && since="$(date -u +"%Y-%m-%d") 00:00"

REPO_DIR="$HOME/repos"
[ -d "$1" ] && REPO_DIR=$1

AUTHOR="Fabian Untermoser"

rg -L "$REPO_DIR" --files --hidden --no-ignore --glob '**/.git/HEAD' 2>/dev/null | sed 's#/.git/HEAD##' |
    while read -r repo; do
        {
            cd "$repo" || die "could not cd to $repo"
            commits=$(git log --oneline --author="$AUTHOR" --pretty="format:- %s" --after="$since")
            if [ -n "$commits" ]; then
                pwd
                echo "$commits"
                echo
            fi
        }
    done
