#!/usr/bin/env bash

QUALITY_SETTINGS=(
    /screen # (screen-view-only quality, 72 dpi images)
    /ebook # (low quality, 150 dpi images)
    /printer # (high quality, 300 dpi images)
    /prepress # (high quality, color preserving, 300 dpi imgs)
    /default # (almost identical to /screen)
)

help() {
    while read -r; do printf '%s\n' "$REPLY"; done <<-EOF
	Usage:
	  ${0##*/} OUTFILE [INFILE1 INFILE2 ..]

	Merge multiple PDFs into one.
	EOF
    exit
}

[ -z "$2" ] && help

printf "merging pdfs into '%s'\n\n" "$1"

# ask user which quality setting should be used.
echo "Choose quality of pdf (lowest to highest quality)"
select quality in "${QUALITY_SETTINGS[@]}"; do
    PDFSETTINGS=${quality}
    break
done

# when run in non-interactive mode
[ -z "$PDFSETTINGS" ] && PDFSETTINGS="/default"

# merging magic
# shellcheck disable=SC2145
gs \
    -sDEVICE=pdfwrite \
    -dCompatibilityLevel=1.4 \
    -dPDFSETTINGS="$PDFSETTINGS" \
    -dNOPAUSE \
    -dQUIET \
    -dBATCH \
    -dDetectDuplicateImages=true \
    -dCompressFonts=true \
    -r150 \
    -sOutputFile="$@"

printf "\nfinished: %s\n\n" "$(du -h "$1")"
