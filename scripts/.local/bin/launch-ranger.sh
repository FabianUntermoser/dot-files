#!/usr/bin/env bash

# USAGE: This script launches ranger with the current wal colors

WAL="${XDG_CONFIG_HOME:-$HOME/.config}/wal-colors.sh"
BASE16="${HOME}/.base16_theme"

declare -a DIRS

# open last used dir
[ -z "$1" ] && LAST_DIR=$(cat "$ZSH_CACHE_DIR/last-working-dir" 2> /dev/null)
[ -d "$LAST_DIR" ] && DIR+=("$LAST_DIR")

[ -z "$1" ] && DIRS=("$(cat "$ZSH_CACHE_DIR/last-working-dir" 2> /dev/null)")

for dir in "$@" ; do
    [ -d "$dir" ] && DIRS+=("$dir")
done

"$TERMINAL" -e "$SHELL" -c "\

    # apply base16 theme, with fallback to wal colors
    [ -f $BASE16 ] && source $BASE16 ||
        { [ -f $WAL ] && source $WAL}

    ranger ${DIRS[*]}"
