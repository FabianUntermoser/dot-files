#!/bin/sh

theme_file="$HOME/.config/zathura/zathurarc-theme"
[ "$(date +%H%M)" -ge "800" ] && # Between 08:00
    [ "$(date +%H%M)" -le "2200" ] && { # And 22:00
    # truncate file
    : > "$theme_file"
} || echo "set recolor" > "$theme_file"
