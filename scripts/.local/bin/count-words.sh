#!/usr/bin/env sh

usage() {
    printf "${0##*/}: print most used words in text"
    exit
}

[ -z "$1" ] && help

file="$1"
tr -c '[:alnum:]' '[\n*]' < "$file" |
    grep -F -v -w -f /usr/share/groff/current/eign |
    sort | uniq -c | sort -n
