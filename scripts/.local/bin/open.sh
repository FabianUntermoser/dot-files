#!/usr/bin/env sh

# usage: open files/directories/urls/stuff

launch(){ setsid -f "$TERMINAL" -e $* ; }

[ -z "$1" ] && exit

for arg; do shift;

    # trim whitespace
    arg=$(echo "$arg" | tr -d '[:space:]')

    [ -f "$arg" ] && launch "$EDITOR" "$arg" && continue
    [ -d "$arg" ] && launch "$SHELL" "-c ranger $arg" && continue


    # handle opening other inputs
    case $arg in
        http*)
            # open url in new window
            [ $BROWSER = "firefox" ] && BROWSER+=" -new-window"
            $BROWSER $arg
            ;;
        *)
            setsid -f xdg-open "$arg"
            ;;
    esac

done
