#!/bin/sh

# USAGE: Open file from cloud
cd "$HOME/cloud" || exit

command rg --files --sort=path | # list cloud files
    rofi -dmenu -i -keep-right -matching glob -p "Open a file" |  # select file
    xargs -I % \
    setsid -f xdg-open "%" # open file with default application
