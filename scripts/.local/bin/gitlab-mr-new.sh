#!/bin/sh

# USAGE: Create a new Merge Request to master for the current branch

GIT_REMOTE=$(git remote get-url origin)
GITLAB_URL=$(echo "$GIT_REMOTE" |
    # convert ssh urls to https
    sed 's#^git@gitlab.com:\(.*\).git#https://gitlab.com/\1#g' |
    # remove .git suffix
    sed 's#.git$##g'
)
CURRENT_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
GIT_SOURCE_BRANCH="$(urlencode.sh "$CURRENT_BRANCH")"
OPT_SOURCE_BRANCH="merge_request%5Bsource_branch%5D=${GIT_SOURCE_BRANCH}"

xdg-open "${GITLAB_URL}/-/merge_requests/new?${OPT_SOURCE_BRANCH}"
