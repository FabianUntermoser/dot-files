#!/usr/bin/env bash

declare -A FORMATS=(
    [vorbis]='ogg'
    [opus]='opus'
    [mp3]='mp3'
    [aac]='m4a'
)

die() { echo "$@" >/dev/stderr; exit 1; }

help() {
    while read -r; do printf '%s\n' "$REPLY"; done <<-EOF
    Usage:
      ${0##*/} FILE

	Extract audio from FILE based on Format.

	Supported Formats:
	$(for key in "${!FORMATS[@]}" ; do
	  printf '%10s -> %s\n' "$key" "${FORMATS[$key]}"
	done)
	EOF
    exit
}

get_audio_format() {
    ffprobe -loglevel error \
        -show_entries stream=codec_name \
        -of csv=p=0 \
        "$1"
}

extract_audio() {
    ffmpeg -i "$1" \
        -y \
        -v quiet \
        -vn \
        -acodec copy \
        "$2"
}

[ "$#" = 0 ] && help

file="$1"
file_ext="${file##*.}"

[ -f "$file" ] || die "not a valid file";

format=$(get_audio_format "$file")
[ -z "$format" ] && die "format not recognized"

ext_audio="${FORMATS[$format]}"
[ -z "$ext_audio" ] && die "extension for audio format $format not known. Please add to the FORMATS array"

output_file="${file/$file_ext/$ext_audio}"
printf "[%s -> %s]\t%s\n" "$file_ext" "$ext_audio" "$output_file"
extract_audio "$file" "$output_file"
