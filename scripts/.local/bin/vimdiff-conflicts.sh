#!/bin/sh

# USAGE: open obsidian sync conflict files in vim diff mode

git status --porcelain >/dev/null 2>&1 || {
    echo error: not in a repository
    exit 1
}

git status -sb |
    grep sync-conflict |
    awk '{ $1=""; print }' |
    while read -r conflict_file; do
        echo "conflict_file $conflict_file"
        file=$(echo "$conflict_file" | sed 's/sync-conflict.*/md"/g')
        echo "file $file"
        # setsid -f "$TERMINAL" -e nvim "$file" "+Gvdiffsplit $conflict_file"
        # setsid -f "$TERMINAL" -e nvim -d -- "$file" "$conflict_file"
        eval nvim -d -- "$file" "$conflict_file"
        # nvim "$file" "+Gvdiffsplit $conflict_file"
        # eval nvim -d -- "$file" "$conflict_file" ;
    done


# git status -s |
#     grep sync-conflict |
#     awk '{ $1=""; print }' |
#     sed 's#^\(.*\)\(sync-conflict-.*.md\)#\1md" \1\2#' |
#     while read -r file conflict ; do
#         setsid -f "$TERMINAL" -e nvim -d "$file" "+Gvdiffsplit $conflict"
#     done
