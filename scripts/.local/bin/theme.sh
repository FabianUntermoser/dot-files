#!/usr/bin/env bash

# variable defaults
: "${XDG_CONFIG_HOME:=$HOME/.config}"
: "${BASE16_SHELL:=$XDG_CONFIG_HOME/base16-shell}"
: "${IMG_DIR:=$HOME/wallpaper}"

die() { echo -e "\033[0;31m$1\033[0m ${*:2}" >&2; exit 1; }
kill() { ps -ef | grep "$1" | grep -v grep | awk '{print $2}' | xargs kill -9 ; }

help() {
    while read -r; do printf '%s\n' "$REPLY"; done <<-EOF
	This script uses 'wal' and 'base16-shell' to set a dark/light terminal theme

	Usage:
	 ${0##*/} [-h] -[d|l] [-w IMG_DIR] [-t THEME]

	Options:
	 -h | --help                 -> show this help
	 -d | --dark                 -> generate dark colors from wallpapers using wal
	 -l | --light                -> generate light colors from wallpapers using wal
	 -w | --wallpaper [IMG_DIR]  -> specify location of wallpapers (defaults to ~/wallpaper)
	 -t | --theme [THEME]        -> apply base16-shell THEME
	EOF
    exit 0;
}

function parse_arguments() {
    while [ $# -gt 0 ]; do
        case $1 in
            -h|--help) shift; help ;;
            -d|--dark) shift; ARG_DARK=true ;;
            -l|--light) shift; ARG_LIGHT=true ;;
            -w|--wallpaper) shift; IMG_DIR="$1"; shift; ;;
            -t|--theme) shift; ARG_THEME="$1"; shift; ;;
            *) die "invalid argument $1"
        esac
    done
}

set_gnome_preferences() {
    if [ "$ARG_DARK" = true ] ; then
        gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
    elif [ "$ARG_LIGHT" = true ] ; then
        gsettings set org.gnome.desktop.interface color-scheme 'prefer-light'
    fi
}

apply_theme_wal() {
    [ -d "$IMG_DIR" ] || die "Directory $IMG_DIR not found"
    random_file="$IMG_DIR/$(ls "$IMG_DIR" | shuf -n 1)"
    echo random file "$random_file"
    if [ "$ARG_DARK" = true ] ; then
        wal -i "$random_file"
    elif [ "$ARG_LIGHT" = true ] ; then
        wal -i "$random_file" -l
    fi
    command -v genzathurarc && genzathurarc
    command -v swaybg && {
        kill swaybg
        setsid -f swaybg -i "$random_file"
    }
}

apply_theme_base16() {
    [ -z "$1" ] && die "No Theme provided"
    [ -f "$BASE16_SHELL/scripts/base16-$1.sh" ] || die "Theme $1 not found"
    base16_magic "$BASE16_SHELL/scripts/base16-$1.sh" "$1"
}

base16_magic() {
  local script=$1
  local theme=$2
  [ -f "$script" ] && . "$script"
  ln -fs "$script" ~/.base16_theme
  export BASE16_THEME=${theme}
  echo -e "if \0041exists('g:colors_name') || g:colors_name != 'base16-$theme'\n  colorscheme base16-$theme\nendif" >| ~/.vimrc_background
  if [[ -n ${BASE16_SHELL_HOOKS:+s} ]] && [[ -d "${BASE16_SHELL_HOOKS}" ]]; then
    for hook in "$BASE16_SHELL_HOOKS"/*; do
      [ -f "$hook" ] && [ -x "$hook" ] && "$hook"
    done
  fi
}

[ -z "$1" ] && help
parse_arguments "$@"
set_gnome_preferences
apply_theme_wal
apply_theme_base16 "$ARG_THEME"
