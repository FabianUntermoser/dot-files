#!/usr/bin/env bash

# take screenshot and parse qr code

text=$(scrot --select --overwrite --exec "zbarimg -q \$f && rm \$f")
notify-send "copied: $text"

codes=$(echo "$text" | sed 's/^QR-Code://')
echo "codes: $codes"

for code in $codes ; do
    case $code in
        otpauth:*) # Example Code: 'otpauth://totp/Epik.com_ederbit?secret=XYZ'
            # extract secret
            secret=$(echo "$code" | sed 's#^otpauth://totp/.*secret=\(\w*\).*$#\1#g')
            # copy to clipboard
            echo "$secret" | xsel -b
            notify-send "Copied QR-Code Secret: $secret"
            exit
            ;;
        http*) # Example Code: 'https://nl.wikipedia.org/'
            xdg-open "$code"
            ;;
    esac
done
