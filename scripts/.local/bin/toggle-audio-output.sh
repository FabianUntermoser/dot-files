#!/usr/bin/env bash

# get available sinks
sinks=$(pactl list sinks | awk ' /Sink #/ {nr=substr($2,2)} /State/{state=$2} /Name/ {name=$2} /Description/ {$1=""; desc=$0; print nr, state, name, desc}')

[ -z "$sinks" ] && {
    notify-send "not other sinks found"
    exit
}

# select new default sink
new_sink=$(echo "$sinks" | awk ' !/RUNNING/ { $2=""; $3=""; print }' | rofi -dmenu -auto-select -i)

# set new default sink
pactl set-default-sink "$(awk '{print $1}' <<< "$new_sink")"
notify-send "changed default output sink to:" "$(awk '{$1=""; $2=""; print $0}' <<< "$new_sink")"
