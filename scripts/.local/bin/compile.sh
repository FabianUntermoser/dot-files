#!/usr/bin/env sh

help() {
    while read -r; do printf '%s\n' "$REPLY"; done <<-EOF
	Usage:
	  ${0##*/} FILE

	Compile File based on Extension or Shebang.

	Supported Extensions:
	 * .c -> compile with gcc
	 * .cpp -> compile with g++
	 * .md -> compile to .pdf with pandoc
	 * .tex -> compile with latexmk
	 * .py -> run with python
	EOF
    exit
}

# compiles based on file extention and shebang
[ -z "$1" ] && help;
file=$(readlink -f "$1"); shift;
dir=$(dirname "$file")
base="${file%.*}"
cd "$dir" || exit

# make file executable
chmod u+x "$file"

latexmk() {
    file="$1"
    args="-quiet -pdf -shell-escape -f"

    # determine if xelatex is required for compilation
    ( head -n5 "$file" | grep -qi 'xelatex' ) && args="$args -pdflatex='xelatex %O %S'"

    # eval - proper parsing of command arguments
    # command - call actual latexmk command
    eval command latexmk "$args" "$file"
}

# shellcheck disable=SC2086
case "$file" in
    *\.java) javac -g "$file" && java -enableassertions "${base##*/}" ;;
    *\.c) gcc $CFLAGS -g "$file" -lm -o "$base".exe && "$base".exe "$@";;
    *\.cpp) g++ $CXXFLAGS -g "$file" -lm -o "$base".exe && "$base".exe "$@";;
    *\.md) pandoc "$file" --pdf-engine xelatex -o "$base".pdf ;;
    *\.tex) latexmk "$file" ;;
    *\.py) python "$file" ;;
    *) sed 1q "$file" | grep "^#\!/" | sed "s/^#\!//" | xargs -r -I % "$file" "$@";;
esac
