#!/bin/sh

# USAGE: url encode string

# url encode string
urlencodepipe() {
  local LANG=C; local c; while IFS= read -r c; do
    case $c in [a-zA-Z0-9.~_-]) printf '%s' "$c"; continue ;; esac
    printf '%s' "$c" | od -An -tx1 | tr ' ' % | tr -d '\n'
  done <<EOF
$(fold -w1)
EOF
  echo
}

printf '%s' "$*" | urlencodepipe
