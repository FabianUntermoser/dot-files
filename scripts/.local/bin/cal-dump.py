#!/usr/bin/env python

# USAGE: Dump ICS calendar events into daily note
#
# EXAMPLES:
#
#   cal-dump.py DATE CAL_FILES...
#
#   cal-dump.py 2023-09-01 ~/cloud/.Calendar-Backup/*.ics
#   ... will dump formatted events into $DIARY_DIR/2023-09-01.md

import os
import argparse
import pytz
from icalendar import Calendar
import recurring_ical_events
from datetime import timedelta, datetime, timedelta
from tabulate import tabulate

## ARGUMENTS

parser = argparse.ArgumentParser(description="Dump ICS calendar events into daily note")
parser.add_argument('DATE', type=str, help='date in yyyy-mm-dd format')
parser.add_argument('CALENDAR_FILES', nargs='+', type=str, help='calendar files')
parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Enable verbose mode')
args = parser.parse_args()

ARG_DATE = datetime.strptime(args.DATE, "%Y-%m-%d")

## UTIL

def read_calendar_file(file_name):
    # Load the .ics file
    with open(file_name, 'r') as f:
        calendar_data = f.read()
    return calendar_data

def get_events_for_date(calendar_data, date):
    c = Calendar.from_ical(calendar_data)
    return recurring_ical_events.of(c).at(date)

def print_event(event):
    try:
        print(f"Summary: {event['SUMMARY']}")
        print(f"Start: {event['DTSTART'].dt}")
        print(f"End: {event['DTEND'].dt}")
        print(f"Location: {event['LOCATION']}")
        pass
    except Exception as e:
        print(type(event))
        raise e

def format_events(events, format='simple'):
    data = []
    for event in events:
        if is_full_day(event):
            # replace start and end date with ---
            data.append(['...', '...', f"**{event['SUMMARY']}**"])
        else:
            data.append([
                event['DTSTART'].dt.strftime("%H:%M"),
                event['DTEND'].dt.strftime("%H:%M"),
                event['SUMMARY']
            ])

    return tabulate(data, headers=["From", "To", "Activity"], tablefmt=format)

def dump_events(file_path, events, match='Highlight::'):
    note = os.path.expanduser(file_path)
    match = 'Highlight::'

    if not events:
        print('events are empty', args.DATE)
        return

    if not os.path.exists(note):
        print(f"Note does not exist: {note}")
        exit(1)

    with open(note, 'r') as file:
        lines = file.readlines()

        header = events.split('\n')[0]
        if header + '\n' in lines:
            print (f"Events already in note: {note}")
            exit(0)

        for i, line in enumerate(lines):
            if line.startswith(match):
                lines[i+1:i+1] = '\n' + formatted_events + '\n'
                break

    with open(note, 'w') as file:
        file.writelines(lines)


def is_full_day(event):
    dtstart = event.get('DTSTART')
    return dtstart and not hasattr(dtstart.dt, 'hour')

## MAIN

events = []

# dump events from each calendar file
for calendar_file in args.CALENDAR_FILES:
    calendar = read_calendar_file(calendar_file)
    events += get_events_for_date(calendar, ARG_DATE.date())

# check if events empy
if not events:
    print('No events found for', args.DATE)
    exit(0)

# sort full day events first, then by date
events = sorted(events, key=lambda event: (
    not is_full_day(event),
    datetime.combine(event.get('DTSTART').dt, datetime.min.time()) if is_full_day(event) else event.get('DTSTART').dt
))

formatted_events = format_events(events)

note = f"{os.getenv('DIARY_DIR', '~/notes/2AREAS/Journal/daily')}/{ARG_DATE.strftime('%Y-%m-%d')}.md"
print('dumping into note:', note)
if not args.verbose: dump_events(note, formatted_events)
print(formatted_events + "\n")
