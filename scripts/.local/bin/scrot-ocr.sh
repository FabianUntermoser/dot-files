#!/usr/bin/env bash

# take screenshot and put text in system clipboard

scrot --select --overwrite --exec "tesseract \$f - | xsel -b && rm \$f"
notify-send "Copied OCR to clipboard"
