#!/bin/bash

# usage: alert if disk size gets low

# df args: Filesystem Size Used Avail Use% Mounted
df | while read -r disk _ _ _ percentage _ ; do
    var="$( echo "$percentage" | cut -d'%' -f1 )"
    if ((var>90)); then
        msg=$(printf "Disk '%s' is %s full " "$disk" "$percentage")
        notify-send "$msg"
    fi
done
