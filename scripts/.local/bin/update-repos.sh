#!/usr/bin/env bash

# TODO:
# - [ ] indent function / saving to file removes colors from output
# - [ ] what to do if git asks something? (i.e add fingerprint to known hosts, ...)
# - [ ] add env var for ssh/git to not ask anything and exit 1
# - [ ] show repos with update problems at bottom and show successful ones at the top
# - [ ] dont block when checking out repo, ask for the next one right away

# TESTS:
# - [ ] what is updated when on a feature branch
# - [ ] what happens to other existing origins?

# spin animation
spin() { spin_animation "$@" & pid_spin=$!; }
spin_end() { kill "$pid_spin"; echo; }
spin_animation() {
    local spinner=( 〡 〢 〣 )
    while true; do
        for i in "${spinner[@]}"; do
            printf "\r==> ${_BOLD}%s %s${_RESET}" "$1" "$i"
            sleep 0.25
        done
    done
}

# color codes
_RED=$(tput setaf 1)
_BLUE=$(tput setaf 4)
_GREEN=$(tput setaf 2)
_GOLD=$(tput setaf 3)
_BOLD=$(tput bold)
_RESET=$(tput sgr0)

declare -a PIDS
declare -A REPO_LOGS=()

error() { echo error: "$@" >&2; }
die() { error "$@"; exit 1; }
indent() { sed 's/^/     /'; }
h1() { echo "==> ${_BOLD}$*${_RESET}"; }
update_remotes() { GIT_TERMINAL_PROMPT=0 git remote update --prune 2>&1; }

help() {
    while read -r; do printf '%s\n' "$REPLY"; done <<-EOF
	Usage:
	  ${0##*/} [-h] [-q] DIRS..
	  Update repositories.
	  Scans DIRS for repositories and updates them.
	  Ignore repositories by adding regex matches to a '.repoignore' file in DIRS.

	Steps:
	  1. Check if repositories are clean.
      2. Dirty repositories can be checked out. (skipped in quite mode)
      3. Clean repos are updated in parallel (shell background jobs).

	Repository Status:
	  ✔ - clean repo
	  x - dirty repo
	  ~ - diverged branches

	Options:
	  -h | --help     -> print this help
	  -q | --quiet    -> don't ask user to checkout dirty repos
	EOF
    exit 0;
}

print_repo() {
    local status="$1"
    local repo="$2"
    printf "  -> ["
    case $status in
        1|'✔'|ok) printf "${_GREEN}%s" '✔'; ;;
        2|'x'|dirty) printf "${_RED}%s" 'x'; ;;
        3|'~'|idk) printf "${_BLUE}%s" '~'; ;;
    esac
    printf "${_RESET}%s ${_GOLD}%s${_RESET}\n" ']' "$repo"
}

parse_arguments() {
    for op in "$@"; do
        case $op in
            -q | --quiet ) QUIET=true; shift; ;;
            -h | --help | -* ) help ;;
            * ) ARGS+=("$op") ;;
        esac
    done

    # defaults
    QUIET=${QUIET:-false}
    ARGS=( "${ARGS[@]:-"$(pwd)"}" )
}

wait_for_networking(){
    until [[ $(ping -q -c 1 -W 1 8.8.8.8) ]]; do
        echo "waiting for network connection ..."
        sleep 3
    done
}

get_ignored_files() {
    # example: echo "DIR1" "DIR2/SUBDIR" "private"

    # ignore repositories that match regex in .repoignore
    [ -f "$1/.repoignore" ] &&
        sed -e '/^#/d' -e '/^$/d' "$1/.repoignore" || # use matches from .repoignore, ignoring comments
        echo "DUMMYTEXT" # required to return some text
}

find_repos() {
    find -L "$1" -name .git -type d -prune -exec dirname {} \; |
        # ignore repositories that match regex in .repoignore
        eval sed "$(printf -- "-e '\#%s#d' " $(get_ignored_files "$1"))"
}

ask() {
    while true; do
        read -p "$* [y/n]: " -n 1 -r answer </dev/tty; echo; echo
        case $answer in
            [Yy]*) return 0 ;;
            [Nn]*) return 1 ;;
        esac
    done
}

checkout_repo() {
    $TERMINAL -e \
        sh -c \
        "cd $1 && \
        git status --short --branch; \
        nvim +G; \
        $SHELL" &>/dev/null
}

update_repo() {
    repo="$1";
    [ -z "$repo" ] && return
    # check if repo is dirty
    if [[ $(git status --porcelain) ]]; then
        print_repo "dirty" "$repo"
        print_git_status
        update_remotes >/dev/null 2>&1 &
        [ "$QUIET" == false ] &&
            ask "     -> checkout?" &&
            checkout_repo "$PWD"
    else
        tmp_file=$(mktemp)
        REPO_LOGS+=([$repo]=$tmp_file);
        # git log --oneline origin/master..
        ( update_remotes; git merge --ff-only "@{u}" ) >> "$tmp_file" 2>&1 &
        PIDS+=("$!")
    fi
}

cleanup_files() {
    for repo in "${!REPO_LOGS[@]}" ; do
        rm "${REPO_LOGS[$repo]}"
    done
}

cleanup(){
    error "Signal trapped - quitting"
    cleanup_files
    exit 0;
}

print_git_status() {
    [ -z "$1" ] || cd "$1" || return
    echo; git status --short --branch | indent; echo;
    [ -z "$1" ] || cd "$OLDPWD" || return
}

clean_log_file() {
    [ -f "$1" ] || return
    sed -i '/Fetching */d' "$1"
    sed -i '/Already up to date/d' "$1"
}

log_commits() {
    local repo="$1"
    local file="${REPO_LOGS[$repo]}"
    [ -f "$file" ] || return 1

    local commit_range
    commit_range=$(sed -nE 's/^Updating //p' "$file")
    [ -z "$commit_range" ] && return 1

    cd "$repo" || return 1
    echo; git log --oneline "$commit_range" | indent; echo
    cd "$OLDPWD" || return 1
    return 0
}

print_log() {
    [ -s "$1" ] &&
        echo && indent < "$1" && echo
}

parse_arguments "$@"

wait_for_networking

# setup cleanup function
trap cleanup 1 2 3 6

h1 "Checking Repositories"

for arg in "${ARGS[@]}"; do

    if [ ! -d "$arg" ]; then
        printf "Argument [${_RED}%s${_RESET}] - is not a directory\n" "$arg"
        continue;
    fi

    while read -r repo; do
        cd "$repo" || continue
        update_repo "$PWD"
        cd "$OLDPWD" || return
    done < <(find_repos "$arg")

done

spin "Updating Repositories"

# shellcheck disable=SC2086
[[ -n ${PIDS[0]} ]] && wait ${PIDS[*]} # wait for all repositories to finish pulling

spin_end

# clean repos
for repo in "${!REPO_LOGS[@]}" ; do
    file="${REPO_LOGS[$repo]}"
    clean_log_file "$file"

    if grep -q "fatal" "$file"; then
        print_repo "~" "$repo"
        echo && indent < "$file"
        print_git_status "$repo"
    else
        print_repo "ok" "$repo"
        log_commits "$repo" || print_log "$file"
    fi

done

cleanup_files
