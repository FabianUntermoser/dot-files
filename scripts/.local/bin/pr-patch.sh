#!/bin/sh

# USAGE: checkout Github PR branch locally
#
#   PR branch URL might be "https://github.com/USER/REPO/tree/BRANCH"

die(){ err "$*"; exit 1; }

[ -z "$1" ] && die "no PR branch URL provided"

PR_BRANCH_URL="$1"
PR_USER="$(echo "$PR_BRANCH_URL" | awk -F'/' '{ print $4 }')"
PR_REPO="$(echo "$PR_BRANCH_URL" | awk -F'/' '{ print $5 }')"
PR_BRANCH="$(echo "$PR_BRANCH_URL" | awk -F'/' '{ print $7 }')"

echo "$PR_USER"
echo "$PR_REPO"
echo "$PR_BRANCH"

# go to repo
cd "$(fasd -d -l "$PR_REPO")" || die "cant find repo $PR_REPO"

# create branch
git checkout -b "${PR_USER}-${PR_BRANCH}" || die "branch $PR_BRANCH already exists"
git pull "git@github.com:${PR_USER}/${PR_REPO}.git" "$PR_BRANCH" || die "cant pull PR"

# open editor
nvim
