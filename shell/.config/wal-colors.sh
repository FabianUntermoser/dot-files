#!/usr/bin/env bash

[ -d "$XDG_CACHE_HOME/wal" ] || return

# fetch colorscheme from wal
(cat "$XDG_CACHE_HOME/wal/sequences" &)

# fix ls text not readable when using pywall
[ -f "$XDG_CACHE_HOME/wal/colors.sh" ] &&
    source "$XDG_CACHE_HOME/wal/colors.sh"
