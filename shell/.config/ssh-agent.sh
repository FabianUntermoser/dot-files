#!/bin/sh

# start ssh-agent

# x11 ssh keychain
if [ -n "$DESKTOP_SESSION" ]; then
    eval "$(gnome-keyring-daemon --start)"
    export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/gcr/ssh"
fi

# wayland ssh keychain
if [ -n "$WAYLAND_DISPLAY" ]; then
    eval "$(ssh-agent)" >/dev/null
fi
