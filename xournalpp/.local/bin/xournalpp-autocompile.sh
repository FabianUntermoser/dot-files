#!/bin/sh

# This script will auto convert xournalpp files to pdf files

die() { echo "$@" 1>/dev/stderr; exit 1; }

[ -z "$1" ] && die "No File provided"

file="$1"
ext=${file##*.}
base=${file%.*}
# ensure compiled pdf does not conflict with original pdf
pdf=${base}-edited.pdf

[ "$ext" = "xopp" ] || die "File does not have .xopp extension"

notify-send "Autocompiling $pdf"

setsid -f xournalpp "$file"

# compile pdf on file changes
echo "$file" | entr -n -p xournalpp -p "$pdf" "$file" &

# monitor running xournalpp process
while true; do
    pgrep -f "xournalpp $file" >/dev/null || break
    sleep 5
done

# stop entr background jobs
pkill -s 0 -x entr
