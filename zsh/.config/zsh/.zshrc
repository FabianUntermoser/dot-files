# vim: set foldmethod=marker foldlevel=0 nomodeline:

## PROFILING START {{{
# Per-command profiling:

# zmodload zsh/datetime
# setopt promptsubst
# PS4='+$EPOCHREALTIME %N:%i> '
# exec 3>&2 2> startlog.$$
# setopt xtrace prompt_subst

# }}}

## CONFIGURATION {{{
# faster mode switching
export KEYTIMEOUT=1

# prevent <C-s> from hanging the terminal
stty -ixon

# command line completion
autoload -U compaudit compinit
zmodload zsh/complist
compinit

# completion selection
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# complete make targets
zstyle ':completion::complete:make::' tag-order targets variables

# complete hidden files
_comp_options+=(globdots)

# Ctrl+Backspace to delete word
bindkey '^H' backward-kill-word

# }}}

## HELPER FUNCTIONS {{{
exists() { hash "$1" 2>/dev/null; }

source_file () {
    [ -r "$1" ] || return 1;
    source "$1"
}

# url encode string
urlencodepipe() {
  local LANG=C; local c; while IFS= read -r c; do
    case $c in [a-zA-Z0-9.~_-]) printf '%s' "$c"; continue ;; esac
    printf '%s' "$c" | od -An -tx1 | tr ' ' % | tr -d '\n'
  done <<EOF
$(fold -w1)
EOF
  echo
}
urlencode() { printf '%s' "$*" | urlencodepipe ;}

# }}}

## ALIASES {{{

# expanded aliases
typeset -a ealiases
ealiases=()

# blank aliases - without whitespace
typeset -a baliases
baliases=()


## setup advanced alias
# -e | create alias that expands
# -b | remove whitespace after expansion
# -* | any other arguments are passed to zsh
create_alias() {
    # parse options
    local _expand _blank _alias_args
    for arg; do
        case $arg in
            -e) shift; _expand=true; ;;
            -b) shift; _blank=true; ;;
            -*) shift; _alias_args+=($arg); ;;
        esac
    done

    # create zsh arg
    args="$@"
    alias ${_alias_args[@]} "$args"
    # get alias keyword
    key=${args%%\=*}
    # trim alias key
    key=${key##* }

    # add to appropriate alias arrays
    [ $_blank ] && baliases+=(${key})
    [ $_expand ] && ealiases+=(${key})
}

expand-alias-space() {
  # check if balias
  [[ $LBUFFER =~ "(^| )(${(j:|:)baliases})\$" ]]; removeSpace=$?

  # expand aliases in ealiases array
  if [[ $LBUFFER =~ "(${(j:|:)ealiases})\$" ]]; then
    zle _expand_alias
  fi

  zle self-insert

  # remove whitespace for baliases
  if [[ "$removeSpace" = "0" ]]; then
    zle backward-delete-char
  fi
}

# setup alias hook
zle -N expand-alias-space
bindkey " " expand-alias-space
bindkey -M isearch " " magic-space
# }}}

## THEME {{{
autoload -U colors && colors # ls colors

# prompt
source_file "$ZDOTDIR/themes/prompt-support.zsh"
source_file "$ZDOTDIR/themes/prompt.zsh-theme"

source_file "$XDG_CONFIG_HOME/wal-colors.sh"
# source_file "$XDG_CONFIG_HOME/base16-colors.sh"
# source_file "$ZDOTDIR/themes/murilasso.zsh-theme"
# source_file "$ZDOTDIR/themes/agnoster.zsh-theme"
#}}}

## PLUGINS {{{
source_file "$XDG_CONFIG_HOME/aliasrc"
source_file "$XDG_CONFIG_HOME/aliasrc-private"
source_file "$XDG_CONFIG_HOME/zsh/aliasrc"
source_file "$XDG_CONFIG_HOME/zsh/fasdrc"
source_file "$ZDOTDIR/plugins/zsh-vim-mode.plugin.zsh"
source_file "/usr/share/fzf/key-bindings.zsh"
source_file "$ZDOTDIR/plugins/key-bindings.plugin.zsh"
# source_file "$ZDOTDIR/plugins/sudo.plugin.zsh"
source_file "$ZDOTDIR/plugins/dircycle.plugin.zsh" && {
    bindkey '^[[1;3D' insert-cycledleft  # Alt+Left
    bindkey '^[[1;3C' insert-cycledright # Alt+Right
}
# }}}

## ZAP Plugin Manager {{{
# See https://github.com/zap-zsh/zap
[ -f "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh" ] && source "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh"
plug "zsh-users/zsh-autosuggestions"
plug "zsh-users/zsh-syntax-highlighting"
# }}}

## COMPLETION {{{
source_file "/usr/share/fzf/completion.zsh"
source_file "$ZDOTDIR/plugins/git-extras.plugin.zsh"
source_file "$ZDOTDIR/plugins/docker.plugin.zsh"

# Load Angular CLI autocompletion.
# exists ng && source <(ng completion script 2>/dev/null)

# Hardhat completion (tabtab source for packages)
source_file "$HOME/.config/tabtab/zsh/__tabtab.zsh"
# }}}

## PROFILING END {{{

# Per-command profiling
# unsetopt xtrace
# exec 2>&3 3>&-

# }}}
