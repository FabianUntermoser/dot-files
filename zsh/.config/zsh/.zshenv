# these things should be available in every zsh instance

[ -z "$XDG_CONFIG_HOME" ] && XDG_CONFIG_HOME="$HOME/.config"

source_file () { [ -r "$1" ] && source "$1"; }

# plugins
source_file "$HOME/.keyboard"
source_file "$XDG_CONFIG_HOME/fzfrc"
source_file "$ZDOTDIR/plugins/last-working-dir.plugin.zsh"
source_file "$ZDOTDIR/plugins/titlebar.plugin.zsh"

# history file
export HISTFILE="$ZDOTDIR"/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000
setopt share_history # share history between sessions
setopt hist_ignore_all_dups # remove old duplicated commands
setopt hist_ignore_space # ignore space prefixed commands
setopt hist_reduce_blanks # remove superfluous blanks

# keep track of switched directories
setopt auto_pushd pushd_ignore_dups

# if command is a dir, cd into it
setopt auto_cd

# dont print pwd when changing dirs (i.e: cd -)
setopt cd_silent

# support prompt substitution required for themes
setopt prompt_subst

# completion list settings
setopt list_packed # smaller lists
setopt menu_complete # insert first match immediately

# fcitx japanese type system
export GTK_IM_MODULE='fcitx'
export QT_IM_MODULE='fcitx'
export SDL_IM_MODULE='fcitx'
export XMODIFIERS='@im=fcitx'
# export XKB_DEFAULT_MODEL=jp106
# export XKB_DEFAULT_LAYOUT=jp,br
# export XKB_DEFAULT_OPTIONS=grp:rctrl_toggle
