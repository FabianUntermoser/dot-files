## ZSH key bindings

# cycle completion keys:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'left' vi-backward-char
bindkey -M menuselect 'down' vi-down-line-or-history
bindkey -M menuselect 'up' vi-up-line-or-history
bindkey -M menuselect 'right' vi-forward-char

# shift+tab cycle completion in vi mode
bindkey '^[[Z' reverse-menu-complete

# alt+e - edit line in vim buffer
autoload -U edit-command-line && zle -N edit-command-line
bindkey '^[e' edit-command-line          # insert mode
bindkey -M vicmd "^[e" edit-command-line # normal mode

# Ctrl+k - clear & ls
clear-ls-all() {
    clear
    l #alias
}
zle -N clear-ls-all
bindkey '^K' clear-ls-all
