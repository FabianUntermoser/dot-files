# vim:set ft=zsh sw=4 et fdm=marker:

# Global aliases can break things. Unset before using any non-builtins.
[[ -o aliases ]] && _vim_mode_shopt_aliases=1
builtin set -o no_aliases

bindkey -v

# ${(%):-%x}_debug () { print -r "$(date) $@" >> /tmp/zsh-debug-vim-mode.log 2>&1 }


# Special keys {{{1

# NB: The terminfo sequences are meant to be used with the terminal
# in *application mode*. This is properly initiated with `echoti smkx`,
# usually in a zle line-init hook widget. But it may cause problems:
# https://github.com/robbyrussell/oh-my-zsh/pull/5113
#
# So for now, leave smkx untouched, and let the framework deal with it.
# Instead, read from terminfo but also hard-code values that various
# terminals use.
#
# Please open an issue if your special keys (Home, End, arrow keys,
# etc.) are not being recognized here!
#
# Extra info:
# http://invisible-island.net/xterm/xterm.faq.html#xterm_arrows
# https://stackoverflow.com/a/29408977/749778

zmodload zsh/terminfo

typeset -A -H vim_mode_special_keys

function vim-mode-define-special-key () {
    local name="$1" tiname="$2"
    local -a seqs
    # Note that (V) uses the "^[" notation for <Esc>, and "^X" for <Ctrl-x>
    [[ -n $tiname && -n $terminfo[$tiname] ]] && seqs+=${(V)terminfo[$tiname]}
    for seq in ${@[3,-1]}; do
        seqs+=$seq
    done
    vim_mode_special_keys[$name]=${${(uOqqq)seqs}}
}

# Explicitly check for VT100 versions (both normal and application mode)
vim-mode-define-special-key Left       kcub1 "^[[D" "^[OD"
vim-mode-define-special-key Right      kcuf1 "^[[C" "^[OC"
vim-mode-define-special-key Up         kcuu1 "^[[A" "^[OA"
vim-mode-define-special-key Down       kcud1 "^[[B" "^[OB"
# These are XTerm and various others
vim-mode-define-special-key Home       khome "^[[1~" "^[[H"
vim-mode-define-special-key End        kend  "^[[4~" "^[[F"
vim-mode-define-special-key PgUp       kpp   "^[[5~"
vim-mode-define-special-key PgDown     knp   "^[[6~"
vim-mode-define-special-key Insert     kich1 "^[[2~"
vim-mode-define-special-key Delete     kdch1 "^[[3~"
vim-mode-define-special-key Shift-Tab  kcbt  "^[[Z"
# These aren't in terminfo; these are for:   XTerm  &  Rxvt
vim-mode-define-special-key Ctrl-Left  ''    "^[[1;5D" "^[Od"
vim-mode-define-special-key Ctrl-Right ''    "^[[1;5C" "^[Oc"
vim-mode-define-special-key Ctrl-Up    ''    "^[[1;5A" "^[Oa"
vim-mode-define-special-key Ctrl-Down  ''    "^[[1;5B" "^[Ob"
vim-mode-define-special-key Alt-Left   ''    "^[[1;3D" "^[^[[D"
vim-mode-define-special-key Alt-Right  ''    "^[[1;3C" "^[^[[C"
vim-mode-define-special-key Alt-Up     ''    "^[[1;3A" "^[^[[A"
vim-mode-define-special-key Alt-Down   ''    "^[[1;3B" "^[^[[B"

#for k in ${(k)vim_mode_special_keys}; do
#    printf '%-12s' "$k:";
#    for x in ${(z)vim_mode_special_keys[$k]}; do printf "%8s" ${(Q)x}; done;
#    printf "\n";
#done


# + vim-mode-bindkey {{{1
function vim-mode-bindkey () {
    local -a maps
    local command

    while (( $# )); do
        [[ $1 = '--' ]] && break
        maps+=$1
        shift
    done
    shift

    command=$1
    shift

    # A key combo can be made of more than one key press, so a binding for
    # <Home> <End> will map to '^[[1~^[[4~', for example. XXX Except this
    # doesn't seem to work. ZLE just wants a single special key for viins
    # & vicmd (multiples work in emacs). Oh, well, this accumulator
    # doesn't hurt and may come in handy. Just only call vim-mode-bindkey
    # with one special key.

    function vim-mode-accum-combo () {
        typeset -g -a combos
        local combo="$1"; shift
        if (( $#@ )); then
            local cur="$1"; shift
            if (( ${+vim_mode_special_keys[$cur]} )); then
                for seq in ${(z)vim_mode_special_keys[$cur]}; do
                    vim-mode-accum-combo "$combo${(Q)seq}" "$@"
                done
            else
                vim-mode-accum-combo "$combo$cur" "$@"
            fi
        else
            combos+="$combo"
        fi
    }

    local -a combos
    vim-mode-accum-combo '' "$@"
    for c in ${combos}; do
        for m in ${maps}; do
            bindkey -M $m "$c" $command
        done
    done
}

if [[ -z $VIM_MODE_NO_DEFAULT_BINDINGS ]]; then
    # Emacs-like bindings {{{1
    vim-mode-bindkey viins vicmd -- beginning-of-line                  '^A'
    vim-mode-bindkey viins vicmd -- backward-char                      '^B'
    vim-mode-bindkey viins vicmd -- end-of-line                        '^E'
    vim-mode-bindkey viins vicmd -- forward-char                       '^F'
    vim-mode-bindkey viins vicmd -- kill-line                          '^K'
    vim-mode-bindkey viins vicmd -- history-incremental-search-backward '^R'
    vim-mode-bindkey viins vicmd -- history-incremental-search-forward  '^S'
    vim-mode-bindkey viins vicmd -- backward-kill-line                 '^U'
    vim-mode-bindkey viins vicmd -- backward-kill-word                 '^W'
    vim-mode-bindkey viins vicmd -- yank                               '^Y'
    vim-mode-bindkey viins vicmd -- undo                               '^_'

    # Avoid key bindings that conflict with <Esc> entering NORMAL mode, like
    # - common movement keys (hljk...)
    # - common actions (dxcr...)
    # But make this configurable: some people will never use ^[b and would
    # rather be sure not to have a conflict, while others use it a lot and will
    # rarely type 'b' as the first key in NORMAL mode. Which behavior shoudl win
    # is very user-dependent.
    vim-mode-maybe-bind() {
        local k="$1"; shift
        if (( $+VIM_MODE_VICMD_KEY )) \
            || [[ ${VIM_MODE_ESC_PREFIXED_WANTED-bdf.g} = *${k}* ]];
        then
            vim-mode-bindkey "$@"
        fi
    }

    vim-mode-maybe-bind b viins vicmd -- backward-word                 '^[b'
    vim-mode-maybe-bind d viins vicmd -- kill-word                     '^[d'
    vim-mode-maybe-bind f viins vicmd -- forward-word                  '^[f'
    vim-mode-maybe-bind h viins       -- run-help                      '^[h'
    # u is not likely to cause conflict, but keep it here with l
    vim-mode-maybe-bind u viins       -- up-case-word                  '^[u'
    vim-mode-maybe-bind l viins       -- down-case-word                '^[l'

    # Some <Esc>-prefixed bindings that should rarely conflict with NORMAL mode,
    # so always define them
    # '.' usually comes after some other keystrokes
    vim-mode-maybe-bind . viins vicmd -- insert-last-word              '^[.'
    # 'g...' bindings are not commonly-used; see `bindkey -pM vicmd g`
    vim-mode-maybe-bind g viins       -- get-line                      '^[g'
    vim-mode-bindkey viins       -- push-line                          '^Q'

    vim-mode-bindkey viins vicmd -- beginning-of-line                  Home
    vim-mode-bindkey viins vicmd -- end-of-line                        End
    vim-mode-bindkey viins vicmd -- backward-word                      Ctrl-Left
    vim-mode-bindkey viins vicmd -- backward-word                      Alt-Left
    vim-mode-bindkey viins vicmd -- forward-word                       Ctrl-Right
    vim-mode-bindkey viins vicmd -- forward-word                       Alt-Right
    vim-mode-bindkey viins vicmd -- up-line-or-history                 PgUp
    vim-mode-bindkey viins vicmd -- down-line-or-history               PgDown

    vim-mode-bindkey viins       -- overwrite-mode                     Insert
    vim-mode-bindkey viins       -- delete-char                        Delete
    vim-mode-bindkey viins       -- reverse-menu-complete              Shift-Tab
    vim-mode-bindkey viins       -- delete-char-or-list                '^D'
    vim-mode-bindkey viins       -- backward-delete-word               '^H'
    vim-mode-bindkey viins       -- backward-delete-char               '^?'
    vim-mode-bindkey viins       -- redisplay                          '^X^R'
    vim-mode-bindkey viins       -- exchange-point-and-mark            '^X^X'

    vim-mode-bindkey       vicmd -- run-help                           'H'
    vim-mode-bindkey       vicmd -- redo                               'U'
    vim-mode-bindkey       vicmd -- vi-yank-eol                        'Y'

    autoload -U edit-command-line
    zle -N edit-command-line
    vim-mode-bindkey viins vicmd -- edit-command-line                  '^X^E'
    vim-mode-bindkey       vicmd -- edit-command-line                  '^V'

    if [[ -n $HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND ]]; then
        vim-mode-bindkey viins vicmd -- history-substring-search-up    '^P'
        vim-mode-bindkey viins vicmd -- history-substring-search-down  '^N'
        vim-mode-bindkey viins vicmd -- history-substring-search-up    Up
        vim-mode-bindkey viins vicmd -- history-substring-search-down  Down
    else
        vim-mode-bindkey viins vicmd -- up-line-or-history             '^P'
        vim-mode-bindkey viins vicmd -- down-line-or-history           '^N'
        vim-mode-bindkey viins vicmd -- up-line-or-history             Up
        vim-mode-bindkey viins vicmd -- down-line-or-history           Down
    fi

    exit-cmd () {exit;}
    zle -N exit-cmd
    vim-mode-bindkey       vicmd -- exit-cmd                           Z Z
    vim-mode-bindkey       vicmd -- exit-cmd                           Z Q


    # Enable surround text-objects (quotes, brackets) {{{1

    autoload -U select-bracketed
    zle -N select-bracketed
    for m in visual viopp; do
        for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
            vim-mode-bindkey $m -- select-bracketed $c
        done
    done

    autoload -U select-quoted
    zle -N select-quoted
    for m in visual viopp; do
        for c in {a,i}{\',\",\`}; do
            vim-mode-bindkey $m -- select-quoted $c
        done
    done

    autoload -Uz surround
    zle -N delete-surround surround
    zle -N change-surround surround
    zle -N add-surround surround
    vim-mode-bindkey vicmd  -- change-surround cs
    vim-mode-bindkey vicmd  -- delete-surround ds
    vim-mode-bindkey vicmd  -- add-surround    ys
    vim-mode-bindkey visual -- add-surround    S


    # Escape shortcut {{{1
    # From http://bewatermyfriend.org/posts/2010/08-08.21-16-02-computer.html
    #   > Copyright (c) 2010, Frank Terbeck <ft@bewatermyfriend.org>
    #   > The same licensing terms as with zsh apply.
    if (( $+VIM_MODE_VICMD_KEY )); then
        vim-mode-bindkey viins -- vi-cmd-mode "$VIM_MODE_VICMD_KEY"

        case $VIM_MODE_VICMD_KEY in
            ^[Dd] )
                builtin set -o ignore_eof
                vim-mode-bindkey vicmd -- vim-mode-accept-or-eof "$VIM_MODE_VICMD_KEY"

                function vim-mode-accept-or-eof() {
                    if [[ $#BUFFER = 0 ]]; then
                        exit
                    else
                        zle accept-line
                    fi
                }
                zle -N vim-mode-accept-or-eof
                ;;
        esac
    fi

    unfunction vim-mode-maybe-bind
fi

# Identifying the editing mode {{{1

if [[ $VIM_MODE_TRACK_KEYMAP != no ]]; then
    # Export the main variable so higher-level processes can view it;
    # e.g., github:starship/starship uses this
    export VIM_MODE_KEYMAP

    autoload -Uz add-zsh-hook
    autoload -Uz add-zle-hook-widget

    # Compatibility with old variable names
    (( $+MODE_INDICATOR_I )) && : ${MODE_INDICATOR_VIINS=MODE_INDICATOR_I}
    (( $+MODE_INDICATOR_N )) && : ${MODE_INDICATOR_VICMD=MODE_INDICATOR_N}
    (( $+MODE_INDICATOR_C )) && : ${MODE_INDICATOR_SEARCH=MODE_INDICATOR_C}

    typeset -g -a vim_mode_keymap_funcs=()

    vim-mode-precmd           () { vim-mode-handle-event precmd           "$KEYMAP" }
    add-zsh-hook precmd vim-mode-precmd

    vim-mode-isearch-update   () { vim-mode-handle-event isearch-update   "$KEYMAP" }
    vim-mode-isearch-exit     () { vim-mode-handle-event isearch-exit     "$KEYMAP" }
    vim-mode-line-pre-redraw  () { vim-mode-handle-event line-pre-redraw  "$KEYMAP" }
    vim-mode-line-init        () { vim-mode-handle-event line-init        "$KEYMAP" }

    () {
        local w; for w in "$@"; do add-zle-hook-widget $w vim-mode-$w; done
    } isearch-exit isearch-update line-pre-redraw line-init

    typeset -g vim_mode_keymap_state=

    vim-mode-handle-event () {
        #${(%):-%x}_debug "handle-event [${(qq)@}][cur:${VIM_MODE_KEYMAP}]"

        local hook="$1"
        local keymap="$2"

        case $hook in
        line-init )
            [[ $VIM_MODE_KEYMAP = vicmd ]] && zle && zle vi-cmd-mode
            ;;
        line-pre-redraw )
            # This hook is called (maybe several times) on every action except
            # for the initial prompt drawing
            case $vim_mode_keymap_state in
            '' )
                vim_mode_set_keymap "$keymap"
                ;;
            *-escape )
                vim_mode_set_keymap "${vim_mode_keymap_state%-escape}"
                vim_mode_keymap_state=
                ;;
            *-update )
                # Normal update in isearch mode
                vim_mode_keymap_state=${vim_mode_keymap_state%-update}
                vim_mode_set_keymap isearch
                ;;
            * )
                # ^C was hit during isearch mode!
                vim_mode_set_keymap "$vim_mode_keymap_state"
                vim_mode_keymap_state=
                ;;
            esac
            ;;
        isearch-update )
            if [[ $keymap = vicmd ]]; then
                # This is an abnormal exit from search (like <Esc>)
                vim_mode_keymap_state+='-escape'
            elif [[ $VIM_MODE_KEYMAP != isearch ]]; then
                # Normal update, starting search mode
                vim_mode_keymap_state=${VIM_MODE_KEYMAP}-update
            else
                # Normal update, staying in search mode
                vim_mode_keymap_state+=-update
            fi
            ;;
        isearch-exit )
            if [[ $VIM_MODE_KEYMAP = isearch ]]; then
                # This could be a normal (movement key) exit, but it could also
                # be ^G which behaves almost like <Esc>. So don't trust $keymap.
                vim_mode_keymap_state+='-escape'
            fi

            # Otherwise, we already exited search via abnormal isearch-update,
            # so there is nothing to do here.
            ;;
        precmd )
            # When the prompt is first shown line-pre-redraw does not get called
            # so the state must be initialized here
            vim_mode_keymap_state=
            vim_mode_set_keymap $(vim-mode-initial-keymap)
            ;;
        * )
            # Should not happen
            zle && zle -M "zsh-vim-mode internal error: bad hook $hook"
            ;;
        esac
    }

    vim_mode_set_keymap () {
        local keymap="$1"

        [[ $keymap = main || $keymap = '' ]] && keymap=viins

        if [[ $keymap = vicmd ]]; then
            local active=${REGION_ACTIVE:-0}
            if [[ $active = 1 ]]; then
                keymap=visual
            elif [[ $active = 2 ]]; then
                keymap=vline
            fi
        elif [[ $keymap = viins ]]; then
            [[ $ZLE_STATE = *overwrite* ]] && keymap=replace
        fi

        #${(%):-%x}_debug "     -> $keymap"

        [[ $VIM_MODE_KEYMAP = $keymap ]] && return

        # Can be used by prompt themes, etc.
        VIM_MODE_KEYMAP=$keymap

        local func
        for func in ${vim_mode_keymap_funcs[@]}; do
            ${func} "$keymap"
        done
    }

    vim-mode-initial-keymap () {
        case ${VIM_MODE_INITIAL_KEYMAP:-viins} in
            last)
                case $VIM_MODE_KEYMAP in
                    vicmd|visual|vline) print vicmd ;;
                    *)                  print viins ;;
                esac
                ;;
            vicmd)
                print vicmd ;;
            *)
                print viins ;;
        esac
    }

    vim-mode-set-cursor-style() {
        local keymap="$1"

        if [[ -n $MODE_CURSOR_VIINS \
           || -n $MODE_CURSOR_REPLACE \
           || -n $MODE_CURSOR_VICMD \
           || -n $MODE_CURSOR_SEARCH \
           || -n $MODE_CURSOR_VISUAL \
           || -n $MODE_CURSOR_VLINE \
           ]]
        then
            case $keymap in
                DEFAULT) set-terminal-cursor-style ;;
                replace) set-terminal-cursor-style ${=MODE_CURSOR_REPLACE-$=MODE_CURSOR_VIINS} ;;
                vicmd)   set-terminal-cursor-style ${=MODE_CURSOR_VICMD} ;;
                isearch) set-terminal-cursor-style ${=MODE_CURSOR_SEARCH-$=MODE_CURSOR_VIINS} ;;
                visual)  set-terminal-cursor-style ${=MODE_CURSOR_VISUAL-$=MODE_CURSOR_VIINS} ;;
                vline)   set-terminal-cursor-style ${=MODE_CURSOR_VLINE-${=MODE_CURSOR_VISUAL-$=MODE_CURSOR_VIINS}} ;;

                main|viins|*)
                         set-terminal-cursor-style ${=MODE_CURSOR_VIINS} ;;
            esac
        fi
    }

    vim-mode-cursor-init-hook() {
        vim-mode-set-cursor-style $(vim-mode-initial-keymap)
    }

    vim-mode-cursor-finish-hook() {
        vim-mode-set-cursor-style DEFAULT
    }

    # See https://github.com/softmoth/zsh-vim-mode/issues/6#issuecomment-413606070
    if [[ $TERM = (dumb|linux|eterm-color) ]] || (( $+KONSOLE_PROFILE_NAME )); then
        # Disable cursor styling on unsupported terminals
        :
    else
        vim_mode_keymap_funcs+=vim-mode-set-cursor-style

        add-zsh-hook        precmd      vim-mode-cursor-init-hook
        add-zle-hook-widget line-finish vim-mode-cursor-finish-hook
    fi
fi

# Restore shell option 'aliases'. This must be the last thing here.
if [[ $_vim_mode_shopt_aliases = 1 ]]; then
   unset _vim_mode_shopt_aliases
   set -o aliases
fi

# VIM Mode Cursor Shape indicator {{{1
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
