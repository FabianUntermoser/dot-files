autoload -U add-zsh-hook
add-zsh-hook chpwd change_titlebar_cwd
add-zsh-hook preexec change_titlebar_cmd

# set titlebar to current directory
change_titlebar_cwd() {
    [[ -t 1 ]] || return
    case $TERM in
        sun-cmd) print -Pn "\e]l$(shorten_cwd)\e\\"
            ;;
        *xterm*|rxvt|(dt|k|E)term|st*) print -Pn "\e]2;$(shorten_cwd)\a"
            ;;
    esac
}

# NOTE: prexec receives the cmd as arguments
# $1 - typed commmand
# $2 - size limited
# $3 - full text
change_titlebar_cmd() { print "\e]2;$(shorten_cwd): $1\a"; }

# update title bar on start

# Define a function to shorten the path
shorten_cwd() {
  echo $PWD | awk -F/ '{for(i=2;i<=NF-1;i++){printf "/%s", substr($i,1,1)}; print "/"$NF}'
}
